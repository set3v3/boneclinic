<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MyProfileController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/

Route::redirect('/', '/login');
Auth::routes(['register' => false]);
Route::post('logged_in', [LoginController::class, 'authenticate'])->name('logged_in');
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', [HomeController::class, 'index']);
    Route::get('dashboard', [HomeController::class, 'index']);
    Route::get('logout', [LoginController::class, 'logout']);

    /** Profile */
    Route::get('myprofile', [MyProfileController::class, 'index'])->name(
        'myprofile.index'
    );
    Route::post('myprofile/update', [MyProfileController::class, 'update'])->name(
        'myprofile.update'
    );
    Route::post('changepassword', [MyProfileController::class, 'passwordupdate'])->name('change.password');


});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
