$(function () {
  $('#search_date').daterangepicker()
  //Date picker
  $('#datepicker').datepicker({
    autoclose: true,
  })

  //Custom radio button show daterange
  $('.customRange').on('ifChecked', function () {
    document.getElementById('customDaterange').style.visibility = 'visible'
  })
  $('.customRange').on('ifUnchecked', function () {
    document.getElementById('customDaterange').style.visibility = 'hidden'
  })

  $('input[name=search_rangedate]').on('ifChanged', function () {
    if ($('input[name=search_rangedate]:checked').val() == 'custom') {
      document.getElementById('customDaterange').style.visibility = 'visible'
    } else {
      document.getElementById('customDaterange').style.visibility = 'hidden'
    }
  })

  if ($('#isCustomSelected').val() == 'custom') {
    document.getElementById('customDaterange').style.visibility = 'visible'
  }
})
