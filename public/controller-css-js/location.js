function displayMap(latitude, longitude, radius, name) {
    $("#dialog").dialog({
        modal: true,
        title: name,
        width: 800,
        height: 600,
        buttons: {
            Close: function () {
                $(this).dialog('close');
            }
        },
        open: function () {
            var mapOptions = {
                center: new google.maps.LatLng(latitude, longitude),
                zoom: 11,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map($("#dvMap")[0], mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude)
              });
            marker.setMap(map);

        var circle = new google.maps.Circle({
            map: map,
            radius: radius,    
            fillColor: '#AA0000'
            });
            
            circle.bindTo('center', marker, 'position');
        }
    });
}
$(function () {
// Fill modal with content from link href
$("#deliveryChargeModal").on("show.bs.modal", function(e) {
    var location_id = $(e.relatedTarget).attr('data-id');
    $('#location_id').val(location_id);

    $('#addeditFrm')[0].reset();

    $('.loading').show()

    $.ajax({
        type: 'get',
        dataType: 'json',
        url: baseUrl + '/locations/' + location_id + '/getdeliverycharge',
        data: {},
        success: function (response) {
          $('.loading').hide();

          if(response.data.capped_delivery == 1)
          $('#capped_delivery').prop('checked', true);
          else
          $('#capped_delivery').prop('checked', false);

          if(response.data.locked == 1)
          $('#locked').prop('checked', true);
          else
          $('#locked').prop('checked', false);

          $('#capped_delivery_charge').val(response.data.capped_delivery_charge);
          $('#initial_distance').val(response.data.initial_distance);
          $('#initial_charge').val(response.data.initial_charge);
          $('#additional_distance').val(response.data.additional_distance);
          $('#additional_charge').val(response.data.additional_charge);
          $('#maximum_distance').val(response.data.maximum_distance);
          $('#default_charge').val(response.data.default_charge);

          displayCappedCharge();
          
        },
        error: function () {
          $('.loading').hide()
        },
      });
});
});

function displayCappedCharge() {
    if($("#capped_delivery").is(':checked'))
        $("#capped_delivery_charge_div").show();  // checked
    else
        $("#capped_delivery_charge_div").hide();
}