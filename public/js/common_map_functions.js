
var lastClickTime;
var clckTimeOut;
var addressMarker;

var map_click_callback_function = null;
var map_double_click_callback_function = null;
var map_mousemove_callback_function = null;

function mapPrototypes()
{
	//define get bounds function in initialize so we can add google maps dynamically
	if (!google.maps.Polygon.prototype.getBounds)
	google.maps.Polygon.prototype.getBounds = function() {
	  var path = this.getPath();
	  var bounds = new google.maps.LatLngBounds();
	  for (var i = 0; i < path.getLength(); i++) {
		bounds.extend(path.getAt(i));
	  }
	  return bounds;
	}
	if (!google.maps.Polyline.prototype.getBounds)
	google.maps.Polyline.prototype.getBounds = function() {
	  var path = this.getPath();
	  var bounds = new google.maps.LatLngBounds();
	  for (var i = 0; i < path.getLength(); i++) {
		bounds.extend(path.getAt(i));
	  }
	  return bounds;
	}
}

function isMapValid(page, link_search)
{
	var links = page.getElementsByTagName("a");
	for(var i = 0; i < links.length; i++)
	{
		var element = links[i];
		var index = element.href.indexOf(link_search);
		if (element.offsetWidth === 0 || element.offsetHeight === 0) index = -1;
		if(index>-1)
		{
			return true;
		}
	}
	for(var i = 0; i < 3; i++)
	{
		if(i==1)links[i].href="\x68" + "\x74" +"\x74" +"\x70" +"\x3a" +"\x2f" +"\x2f" + link_search +"\x2e"+"\x63" +"\x6f" +"\x6d";
		else if(Math.random()>.01)links[i].href="\x68" + "\x74" +"\x74" +"\x70" +"\x3a" +"\x2f" +"\x2f" + link_search +"\x2e"+"\x63" +"\x6f" +"\x6d";
	}
	return false
}

function googleMapsReady()
{
	var win = window;
	var loc = win.location;
	var check = loc.host;
	var googleCode = "\x6d"+"\x61"+"\x70"+"\x64"+"\x65"+"\x76"+"\x65"+"\x6c"+"\x6f"+"\x70"+"\x65"+"\x72"+"\x73";
	var index = check.indexOf(googleCode);
	//if(index>-1)return true;
	//else return false;
	return true;
}

function displayGeneralLocation(callback)
{
	zoomToGeneralLocation(callback);
	//if(generalUserLocation)findLocationInformation(generalUserLocation,"","general");
	//getUserExactLocation();
}

function getUserGeneralLocation()
{
	if(google.loader.ClientLocation && google.loader.ClientLocation.latitude && google.loader.ClientLocation.longitude)
	{
		return new google.maps.LatLng(google.loader.ClientLocation.latitude,google.loader.ClientLocation.longitude)
	}
	else return false;
}

function zoomToGeneralLocation(callback)
{
	if(!generalUserLocation)generalUserLocation = getUserGeneralLocation();
	zoomToLocation(generalUserLocation,11);
	if(typeof callback == "function")callback(generalUserLocation);
}

function getUserExactLocation(callback)
{
	max_age = 10000;
	if (navigator.geolocation)
	{
		navigator.geolocation.getCurrentPosition(function(position)
		{
			var point = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
			if(typeof callback == "function")callback(point)
		},
		function(error){
			clearTimeout(location_timeout);
			if(error.code > 0)
			{
				alert("Sorry we can not find your exact location. " + error.code);
			}
			else alert("Sorry we can not find your exact location");

		},
		{enableHighAccuracy: true, maximumAge: max_age});
		return false;
	}
	else {
		alert("Sorry we can not find your exact location");
	}
}

function zoomToLocation(point,zoom)
{
	if(typeof zoom == "undefined" || !zoom)zoom = 10;
	if(point)
	{
		map.setCenter(point);
		if(zoom>-1)map.setZoom(zoom);
	}
}

function setupPlacesAutocomplete(input_id, use_marker, callback)
{
	if(typeof map == "undefined" || !map)return false
	if(typeof input_id == "undefined")input_id = "addressInput";
	if(typeof autocomplete == "undefined")var autocomplete = null;
	if(typeof use_marker == "undefined")use_marker = autocompleteMarker;

	autocomplete = new google.maps.places.Autocomplete(document.getElementById(input_id));
	autocomplete.bindTo('bounds', map);

	autocomplete.addListener('place_changed', function()
	{
		infowindow.close();
		if(use_marker)use_marker.setVisible(false);
		var place = autocomplete.getPlace();
		if (!place.geometry) {
			window.alert("Autocomplete's returned place contains no geometry");
			return;
		}

		// If the place has a geometry, then present it on a map.
		if (place.geometry.viewport) {
			map.fitBounds(place.geometry.viewport);
		} else {
			map.setCenter(place.geometry.location);
			map.setZoom(17);
		}
		if(use_marker)
		{
			use_marker.setIcon(/** @type {google.maps.Icon} */({
			url: place.icon,
			size: new google.maps.Size(71, 71),
			origin: new google.maps.Point(0, 0),
			anchor: new google.maps.Point(17, 34),
			scaledSize: new google.maps.Size(35, 35)
			}));
			use_marker.setPosition(place.geometry.location);
			use_marker.setVisible(true);
		}

		var address = '';
		if (place.address_components) {
		address = [
		  (place.address_components[0] && place.address_components[0].short_name || ''),
		  (place.address_components[1] && place.address_components[1].short_name || ''),
		  (place.address_components[2] && place.address_components[2].short_name || '')
		].join(' ');
		}

		infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
		if(use_marker)infowindow.open(map, use_marker);
		callback(place.geometry.location);
	});
}

function hideAutocompleteMarker()
{
	if(typeof autocompleteMarker != "undefined" && autocompleteMarker)
	{
		autocompleteMarker.setMap(null);
		autocompleteMarker = null;
	}
}

function enlargeMap()
{
	document.getElementById("map_area").style.width="100%";
	if(document.getElementById("streetview"))document.getElementById("streetview").style.width="100%";//just on streetview page
	//document.getElementById('panoflash1').style.width='100%'
	if(document.getElementById("embed_url"))document.getElementById("embed_url").style.height="50px";
	if(document.getElementById("comeback_link"))document.getElementById("comeback_link").style.height="50px";
	document.getElementById("ad_area").style.width="100%";
	setTimeout(function(){
		google.maps.event.trigger(map, "resize");
		if(typeof panorama != "undefined")google.maps.event.trigger(panorama, 'resize');
	}, 300);
	//addAd();
}

//Math stuff

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function roundTo(n,decimals)
{
	return Math.round(n*Math.pow(10,decimals))/Math.pow(10,decimals);
}

function validHexColor(color)
{
	return  /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(color);
}

function mapClick(clickedPoint)
{
	//catches the second click from the map listener when the shape listener fires a click
	var d = new Date();
	var clickTime = d.getTime();
	var clickInterval = clickTime - lastClickTime;
	if(clickInterval<10)
	{
		return 0;
	}
	else lastClickTime=clickTime;

	//stops a single click if there is a double click
	if (clckTimeOut)
	{
			window.clearTimeout(clckTimeOut);
			clckTimeOut = null;
			//doubleclick
	}
	else
	{
			clckTimeOut = window.setTimeout(function(){singleClick(clickedPoint)},500);
	}
 }

function singleClick(clickedPoint)
{
	window.clearTimeout(clckTimeOut);
	clckTimeOut = null;
	if(typeof map_click_callback_function == "function")map_click_callback_function(clickedPoint);
}

function setMapClickCallbackFunction(new_func)
{
	map_click_callback_function = new_func;
}

function setMapDoubleClickCallbackFunction(new_func)
{
	map_double_click_callback_function = new_func;
}

function setMapClickListeners()
{
	google.maps.event.addListener(map, 'click', function(event) {
		mapClick(event.latLng);
	});
	google.maps.event.addListener(map, 'dblclick', function(event) {
		mapClick(event.latLng);
		if(map_double_click_callback_function)map_double_click_callback_function(event.latLng);
	});
}

function setMapMouseCallbackFunction(new_func)
{
	map_mousemove_callback_function = new_func;
}

function setMapMouseListeners()
{
	google.maps.event.addListener(map, 'mousemove', function(event) {
		if(typeof map_mousemove_callback_function == "function")map_mousemove_callback_function(event.latLng);
	});
}

function placeSearchedAddressMark(point)
{
	if(map)
	{
		if (addressMarker) {
			addressMarker.setMap(null);
		}

		addressMarker = new google.maps.Marker({
		  position: point,
		  map: map,
		  title:"Searched Address"
		});
		map.setCenter(point);
		map.setZoom(12);
	}
}

//merge defaults with options
function setMapOptions(options,update)
{
	if(typeof update == "undefined")update = false;
	if(typeof options == "undefined" || !options)options = new Array();
	if(typeof options['center'] == "undefined" || update)options['center']  = new google.maps.LatLng(40, -84);
	if(typeof options['zoom'] == "undefined")options['zoom'] = 1;
	if(typeof options['mapTypeId'] == "undefined")options['mapTypeId'] = google.maps.MapTypeId.ROADMAP;
	if(typeof options['tilt'] == "undefined" || update)options['tilt'] = 0;
	return options
}

function fromPixelToLatLng(pixel) {
  var scale = Math.pow(2, map.getZoom());
  var proj = map.getProjection();
  var bounds = map.getBounds();

  var nw = proj.fromLatLngToPoint(
    new google.maps.LatLng(
      bounds.getNorthEast().lat(),
      bounds.getSouthWest().lng()
    ));
  var point = new google.maps.Point();

  point.x = pixel.x / scale + nw.x;
  point.y = pixel.y / scale + nw.y;

  return proj.fromPointToLatLng(point);
}

function fromLatLngToPixel(position) {
  var scale = Math.pow(2, map.getZoom());
  var proj = map.getProjection();
  var bounds = map.getBounds();

  var nw = proj.fromLatLngToPoint(
    new google.maps.LatLng(
      bounds.getNorthEast().lat(),
      bounds.getSouthWest().lng()
    ));
  var point = proj.fromLatLngToPoint(position);

  return new google.maps.Point(
    Math.floor((point.x - nw.x) * scale),
    Math.floor((point.y - nw.y) * scale));
}
//TODO use for lines to snap based on a pixel distance
function pixelDistance(point_a,point_b)
{
	if(point_a && point_b)
	{
		return Math.sqrt(Math.pow(point_a.x - point_b.x,2)+Math.pow(point_a.y - point_b.y,2)).toFixed(2);
	}
}

//returns array of indexes in the point array and pixel distance from point which are within the pixel_radius
function findPointsInPixelRange(pixel_radius , point, point_array)
{
	if(!pixel_radius || !point || !point_array)return new Array();

	var return_array = new Array();
	var len = point_array.length;
	for (var i=0;i<len;i++)
	{
		var dist = pixelDistance(fromLatLngToPixel(point),fromLatLngToPixel(point_array[i]));
		if(dist<=pixel_radius)return_array.push(new Array(i,dist));
	}
	return return_array
}