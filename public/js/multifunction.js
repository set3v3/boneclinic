var geocoder = null;
var map = null;
var circles = new Array();
var active_circle = null;
var loaded = false;
var click_callback_function = null;
if(typeof currentTool == "undefined")var currentTool = null;
var geocodeService = null;

function init() {
	var mapDiv = document.getElementById('map_canvas');
	var data_loaded = false;
	if(typeof map_options == 'undefined'){
		map_options = {};
	}

	map = new google.maps.Map(mapDiv, setMapOptions(map_options));
	mapPrototypes();
	geocoder = new google.maps.Geocoder();
	setMapClickCallbackFunction(map_click_function);
	setMapDoubleClickCallbackFunction(map_double_click_function);
	setMapClickListeners();
	setMapMouseCallbackFunction(map_mousemove_function)
	setMapMouseListeners();
	initiateDrawingsArray();
	if(typeof input_circles != "undefined" && input_circles)
	{
		loadShapes(input_circles,null);
		data_loaded = true;
	}
	if(typeof input_polygons != "undefined" && input_polygons)
	{
		loadShapes(null,input_polygons);
		data_loaded = true;
	}
	if(typeof input_polylines != "undefined" && input_polylines)
	{
		loadShapes(null,null,input_polylines);
		data_loaded = true;
	}
	loaded=true;
	startDM();
	if(currentTool == "polygon" && !data_loaded)setDrawingMode(currentTool);
	if(currentTool == "polyline" && !data_loaded)setDrawingMode(currentTool);
	if(typeof page_url != "undefined")displaySaveLink(page_url,"comeback_link");

}

var map_click_function = function(clickedPoint){
	if(currentTool == "circle")createCircle(clickedPoint,getInputRadius(),{'editable':true,'draggable':true});
	if(currentTool == "polyline" && mouse_draw)
	{
		mouseTraceClick(clickedPoint);
	}
}

var map_double_click_function = function(clickedPoint){
	if(currentTool == "polyline" && mouse_draw)
	{
		addToWorkingLine(clickedPoint);
		saveWorkingLine();
	}
}

var map_mousemove_function = function(clickedPoint){
	drawMouseTraceLine(clickedPoint);
}

function getLocation(address, callback){
	if (geocoder) {
		geocoder.geocode({ 'address': address}, callback);
	}
	else{
		console.error("Geocoder is null.")
	}
}

function showAddress(addressSelector) {
	var radius;
	var address = $(addressSelector).val();
	if(typeof GeocodeService != "undefined")
	{
		if(!geocodeService)
		{
			try { geocodeService = new GeocodeService();}
			catch(error) { geocodeService = Object.create(GeocodeService.prototype);}//for IE
		}
		geocodeService.geocode(null, address, "", function(location_array, source) {
			if (typeof location_array != "undefined" && typeof location_array.lat != "undefined"){
				var point = new google.maps.LatLng(location_array.lat, location_array.lng);
				showAddressPoint(point);
			}
			else showAddressGoogle(address);
    	});
	}
	else showAddressGoogle(address);
}

function showAddressGoogle(address)
{
	var radius;
	if(typeof address != "undefined" && address)
	{
		getLocation(address, function(response, status) {
			if (!response || status != google.maps.GeocoderStatus.OK)
			{
				alert(address + " not found");
			}
			else
			{
				var l = response[0];//choose first location
				point = l.geometry.location;
				showAddressPoint(point);
			}
		});
	}
}

function showAddressPoint(point)
{
	if(currentTool == "circle")
	{
		radius = getInputRadius();
		if(radius){
			createCircle(point, radius, {'editable':true,'draggable':true});
			zoomToShape(current_shape);
		}
	}
	else placeSearchedAddressMark(point);
}

function createCircleTool(map,center,name)
{
	if(!radius){
		return false;
	}

	var distanceWidget = new DistanceWidget(map, center,name,arguments[3]);

	google.maps.event.addListener(distanceWidget, 'distance_changed', function() {
	  displayInfo(distanceWidget);
	  setInputRadius(distanceWidget)
	});

	google.maps.event.addListener(distanceWidget, 'position_changed', function() {
	  displayInfo(distanceWidget);
	});

	circles.push(distanceWidget);

	if(active_circle)active_circle.set("active",false);

	active_circle = distanceWidget;
	saveLink();
	if(loaded && circles.length==1)zoomToAllCircles();
}

function createInitialCircles(map, circles)
{
	len = circles.length;
	for(i=0;i<len;i++)
	{
		circle = circles[i];
		point = new google.maps.LatLng(circle[1], circle[2]);
		createCircleTool(map,point,"",circle[0]);
		modifyActiveCircle(circle[0],circle[3],circle[4],circle[5])
	}
	loaded = true;
	zoomToAllCircles();
}

function toggleMapSize(mapSelector, context){
	var mapArea = $(mapSelector),
		isLarge = mapArea.hasClass('expand_map'),
		width;

	if(isLarge){
		$('#ad-container-column').css('margin-left', '');
		width = "100%";
	}
	else{
		$('#ad-container-column').css('margin-left', '125px');
		width = (mapArea.width() + 255) + 'px';
	}

	mapArea.toggleClass('expand_map');
	mapArea.css('width', width);
	google.maps.event.trigger(map, "resize");

	if(context){
		if(isLarge){
			context.innerText = 'Enlarge Map';
		}
		else{
			context.innerText = 'Shrink Map';
		}
	}
}

function zoomToAddress(addressSelector){
	var address = $(addressSelector).val();
	if(typeof GeocodeService != "undefined")
	{
		if(!geocodeService)
		{
			try { geocodeService = new GeocodeService();}
			catch(error) { geocodeService = Object.create(GeocodeService.prototype);}//for IE
		}
		geocodeService.geocode(null, address, "", function(location_array, source) {
			if (typeof location_array != "undefined" && typeof location_array.lat != "undefined"){
				var point = new google.maps.LatLng(location_array.lat, location_array.lng);
				zoomToLocation(point);
			}
			else zoomToAddressGoogle(address);
    	});
	}
	else zoomToAddressGoogle(address);
}

function zoomToAddressGoogle(address)
{
	if(typeof address != "undefined" && address)
	{
		getLocation(address, function(response, status) {
			if (!response || status != google.maps.GeocoderStatus.OK)
			{
				alert(address + " not found");
			}
			else
			{
				var l = response[0]; //choose first location
				point = l.geometry.location;
				zoomToLocation(point);
			}
		});
	}
}