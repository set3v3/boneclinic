/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function () {

    'use strict';
    $('ul.Overview li:first').addClass('active');
    $('ul.service-overview li:first').addClass('active');
    // Make the dashboard widgets sortable Using jquery UI
    $('.connectedSortable').sortable({
        placeholder: 'sort-highlight',
        connectWith: '.connectedSortable',
        handle: '.box-header, .nav-tabs',
        forcePlaceholderSize: true,
        zIndex: 999999
    });
    $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');

    // jQuery UI sortable for the todo list
    $('.todo-list').sortable({
        placeholder: 'sort-highlight',
        handle: '.handle',
        forcePlaceholderSize: true,
        zIndex: 999999
    });

    // bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5();

    $('.daterange').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment()
    }, function (start, end) {
        window.alert('You chose: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    });

    /* jQueryKnob */
    $('.knob').knob();

    // jvectormap data
    var visitorsData = {
        US: 398, // USA
        SA: 400, // Saudi Arabia
        CA: 1000, // Canada
        DE: 500, // Germany
        FR: 760, // France
        CN: 300, // China
        AU: 700, // Australia
        BR: 600, // Brazil
        IN: 800, // India
        GB: 320, // Great Britain
        RU: 3000 // Russia
    };
    // World map by jvectormap
    $('#world-map').vectorMap({
        map: 'world_mill_en',
        backgroundColor: 'transparent',
        regionStyle: {
            initial: {
                fill: '#e4e4e4',
                'fill-opacity': 1,
                stroke: 'none',
                'stroke-width': 0,
                'stroke-opacity': 1
            }
        },
        series: {
            regions: [
                {
                    values: visitorsData,
                    scale: ['#92c1dc', '#ebf4f9'],
                    normalizeFunction: 'polynomial'
                }
            ]
        },
        onRegionLabelShow: function (e, el, code) {
            if (typeof visitorsData[code] != 'undefined')
                el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
        }
    });

    // Sparkline charts
    var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
    $('#sparkline-1').sparkline(myvalues, {
        type: 'line',
        lineColor: '#92c1dc',
        fillColor: '#ebf4f9',
        height: '50',
        width: '80'
    });
    myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
    $('#sparkline-2').sparkline(myvalues, {
        type: 'line',
        lineColor: '#92c1dc',
        fillColor: '#ebf4f9',
        height: '50',
        width: '80'
    });
    myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
    $('#sparkline-3').sparkline(myvalues, {
        type: 'line',
        lineColor: '#92c1dc',
        fillColor: '#ebf4f9',
        height: '50',
        width: '80'
    });

    // The Calender
    $('#calendar').datepicker();

    // SLIMSCROLL FOR CHAT WIDGET
    $('#chat-box').slimScroll({
        height: '250px'
    });


    // Fix for charts under tabs
    $('.box ul.nav a').on('shown.bs.tab', function () {
        area.redraw();
        donut.redraw();
        line.redraw();
    });

    /* The todo list plugin */
    $('.todo-list').todoList({
        onCheck: function () {
            window.console.log($(this), 'The element has been checked');
        },
        onUnCheck: function () {
            window.console.log($(this), 'The element has been unchecked');
        }
    });

    /* Date Range picker */
    
    var start = moment().subtract(29, 'days');
    var end = moment();
    /* overview daterange */
    function cb(start, end) {
       //$('#overview_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
       $('#overview_range span').html(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
       var selectedLi = $('ul.Overview li.active a').attr('href');
       //console.log($('ul.Overview li.active a').attr('href'));
       //console.log(start+' '+end);
       if(selectedLi != undefined)
            populateGraphs('overview',selectedLi.substr(1),start,end);
       else
       {
           $('ul.Overview li:first').addClass('active');
           populateGraphs('overview','revenue-chart',start,end);
       }
    }

    $('#overview_range').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'This Year': [moment('01-01-2018'), moment()],
           'Last Year' : [moment('01-01-2017'), moment('12-31-2017')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           //'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

    cb(start, end);
    /* End */

    /* top 10 customer */
    function cbCustomer(start, end) {
       $('#customers_range span').html(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
       customerDounoughtChart();
       
    }

    $('#customers_range').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'This Year': [moment('01-01-2018'), moment()],
           'Last Year' : [moment('01-01-2017'), moment('12-31-2017')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           //'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cbCustomer);
    cbCustomer(start, end);

    /* End */ 
    
    /* Shipment status date range */
    
    function cbShipmentStatus(start, end) {
       $('#shipment_status_range span').html(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
       $(".airShip .num a").each(function () {
            $(this).text(Math.floor((Math.random() * 10000) + 1));
       });
       
    }

    $('#shipment_status_range').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'This Year': [moment('01-01-2018'), moment()],
           'Last Year' : [moment('01-01-2017'), moment('12-31-2017')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           //'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cbShipmentStatus);
    cbShipmentStatus(start, end);
    
    /* End */
    
    /* Order by destination country date range */
    
    function cbOrderCountry(start, end) {
       $('#order_destinationcountry_range span').html(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
       countryDataBarChart();
       
    }

    $('#order_destinationcountry_range').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'This Year': [moment('01-01-2018'), moment()],
           'Last Year' : [moment('01-01-2017'), moment('12-31-2017')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           //'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cbOrderCountry);
    cbOrderCountry(start, end);
    
    /* End */
    
    /* Service overview date range */
    
    function cbServiceOverview(start, end) {
       $('#service_overview_range span').html(start.format('D/M/YYYY') + ' - ' + end.format('D/M/YYYY'));
       var selectedLi = $('ul.service-overview li.active a').attr('href');
       //console.log(selectedLi);
       populateGraphs('service_overview',selectedLi.substr(1),start,end);
       
    }

    $('#service_overview_range').daterangepicker({
        startDate: start,
        endDate: end,
        ranges: {
           'This Year': [moment('01-01-2018'), moment()],
           'Last Year' : [moment('01-01-2017'), moment('12-31-2017')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           //'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cbServiceOverview);
    cbServiceOverview(start, end);
    
    /* End */

    $('ul.Overview li.overview-tab a').click(function () {
        $('ul.Overview li.overview-tab').removeClass('active');
        $(this).addClass('active');
        var selectedLi = $(this).attr('href').substr(1);
        var dateRange = $('#overview_range span').html();
        //console.log(dateRange);
        var dateParts = dateRange.split(" - ");
        var startParts =  dateParts[0].split('/');
        var endParts = dateParts[1].split('/');
        var startDate = new Date(startParts[1]+","+startParts[0]+","+startParts[2]);
        var endDate = new Date(endParts[1]+","+endParts[0]+","+endParts[2]);
        //console.log(startDate+' '+endDate);
        //console.log('click'+selectedLi);
        populateGraphs('overview',selectedLi,moment(startDate),moment(endDate));

    });

    $('ul.service-overview li.service-overview-tab').click(function () {
        $('ul.service-overview li.service-overview-tab').removeClass('active');
        $(this).addClass('active');
        var liId = $(this).find('a').attr('href').substr(1);
        var dateRange = $('#service_overview_range span').html();
        var dateParts = dateRange.split(" - ");
        var startParts =  dateParts[0].split('/');
        var endParts = dateParts[1].split('/');
        var startDate = new Date(startParts[1]+","+startParts[0]+","+startParts[2]);
        var endDate = new Date(endParts[1]+","+endParts[0]+","+endParts[2]);
        populateGraphs('service_overview',liId,moment(startDate),moment(endDate));
    });


    $('ul.country-filter-overview li a').click(function () {
        var selectedLiCountry = $(this).text();
        $(this).parents('.myDropDn').find('button').html(selectedLiCountry + '<span class="caret skybg m-l-10"></span>');
        var selectedLi = $('ul.Overview li.active a').attr('href').substr(1);
        var dateRange = $('#overview_range span').html();
        var dateParts = dateRange.split(" - ");
        var startParts =  dateParts[0].split('/');
        var endParts = dateParts[1].split('/');
        var startDate = new Date(startParts[1]+","+startParts[0]+","+startParts[2]);
        var endDate = new Date(endParts[1]+","+endParts[0]+","+endParts[2]);
        populateGraphs('overview',selectedLi,moment(startDate),moment(endDate));


    });

    $('ul.service-date-filter li a').click(function () {
        var selectedLiOrderwiseDate = $(this).text();
        $(this).parents('.myDropDn').find('button').html(selectedLiOrderwiseDate + '<span class="caret skybg m-l-10"></span>');
        var selectedSection = $(this).parents('.service-overview').find('li.active a').attr('href');
        overviewBarchart(selectedSection.substr(1), selectedLiOrderwiseDate);
    });


});
var chartLabels = [];
function populateGraphs(section,subSection,startDate,endDate)
{
    //console.log(startDate.format('MMMM D, YYYY'));
    var labels = getLabels(startDate,endDate);
    //console.log(chartLabels);
    if(section == 'overview')
    {
        overviewCharts(subSection);
    }
    else if(section == 'service_overview')
    {
        overviewBarchart(subSection);
    }

}

function getLabels(startDate, endDate)
{
    //console.log(startDate.format('MMMM D, YYYY'));
    chartLabels = [];
    dayDifference = endDate.diff(startDate,'days');
    //console.log(dayDifference);
    var currDate = moment(startDate).startOf('day');
    var lastDate = moment(endDate).startOf('day');
    if(dayDifference<=7)
    {        
        while(currDate.diff(lastDate) <= 0) {
            chartLabels.push(parseInt(currDate.month()+1)+' / '+currDate.date());
            currDate.add(1, 'days');
            //dates.push(currDate.clone().toDate());
        }
    }
    else if(dayDifference>7 && dayDifference<=30)
    {
        while(lastDate.diff(currDate,'days') >= 7) {
            //console.log(lastDate.diff(currDate,'days'));
            var endOfWeek = moment(currDate);
            endOfWeek.add(6,'days');
            var label = parseInt(currDate.month()+1)+' / '+currDate.date()+ ' - '+ parseInt(endOfWeek.month()+1)+' / '+endOfWeek.date()
            chartLabels.push(label);
            currDate.add(7, 'days');
        }
        chartLabels.push(parseInt(currDate.month()+1)+' / '+currDate.date()+ ' - '+ parseInt(lastDate.month()+1)+' / '+lastDate.date());

    }
    else if(dayDifference>30 && dayDifference<365)
    {
        while(lastDate.diff(currDate,'months') >0) {
            //console.log(lastDate.diff(currDate,'months'));
            chartLabels.push(currDate.format('MMMM'));
            currDate.add(1,'months');

        }
        chartLabels.push(lastDate.format('MMMM'));
    }
    return true;

}
/* dynamic date config */
var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
var d = new Date();

var xAxisLabel = '';
function getMonths(year)
{
    var monthsThisYear = [];
    for (var i = 0; i < 12; i++)
    {
        monthsThisYear.push(MONTHS[i]);
        if (i == d.getMonth())
        {
            break;
        }
    }

    return monthsThisYear;
}

function getChartLabels(dateFilter)
{
    dateFilter = dateFilter.trim();
    chartLabels = [];
    if (dateFilter == 'This Year') {
        chartLabels = getMonths(d.getFullYear());
        xAxisLabel = 'Month';
    }
    else if (dateFilter == 'Previous Year') {
        chartLabels = MONTHS;
        xAxisLabel = 'Month';
    }
    else if (dateFilter == 'This Month' || dateFilter == 'Previous Month') {
        chartLabels = ['Week 1', 'Week 2', 'Week 3', 'Week 4'];
        xAxisLabel = 'Week';
    }
    else {
        var firstday = new Date(d.setDate(d.getDate() - d.getDay()));
        var lastday = new Date(d.setDate(d.getDate() - d.getDay() + 6));
        while (firstday <= lastday) {
            chartLabels.push((firstday.getMonth() + 1) + ' / ' + firstday.getDate())
            var newDate = firstday.setDate(firstday.getDate() + 1);
            firstday = new Date(newDate);
        }
        xAxisLabel = 'Day';
    }

    return true;

}
/* End */


/* Revenue line graph config */

var config = {
    type: 'line',
    data: {
        labels: getMonths(d.getFullYear()),
        datasets: [{
                label: 'Total sales',
                backgroundColor: window.chartColors.red,
                borderColor: window.chartColors.blue,
                data: [10, 45, 21, 35, 15, 45, 50],
                fill: false,
            }]
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: 'This year revenue'
        },
        legend: {
         onHover: function(e) {
            e.target.style.cursor = 'pointer';
         }
       },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true,
            onHover: function(e) {
            var point = this.getElementAtEvent(e);
            if (point.length) e.target.style.cursor = 'pointer';
            else e.target.style.cursor = 'default';
          }
        },
        scales: {
            xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Month'
                    }
                }],
            yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Revenue Generate'
                    }
                }]
        }
    }
};

/* config end */

/* dynamic overview line charts */

function overviewCharts(selectedLi) {
    //console.log(chartLabels);
    var selectedLiCountry = $('.country-filter-overview').parents('.myDropDn').find('button').text();
    var ctx = document.getElementById(selectedLi + '-data').getContext('2d');

    //console.log(selectedLi+' '+selectedLiCountry);
    config.data.labels = chartLabels;
    /*config.options.scales.xAxes = [];
    config.options.scales.xAxes.push({
        display: true,
        scaleLabel: {
            display: true,
            labelString: xAxisLabel
        }
    });*/
    if (selectedLi == 'revenue-chart')
    {
        config.options.title.text = "Revenue";
        config.data.datasets = [];

        config.data.datasets.push({
            label: 'Total Revenue',
			behaveLikeLine: true,
            backgroundColor: '#009954',
            borderColor: window.chartColors.blue,
            data: [
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
            ],
            fill: false,
        });
    }
    else if (selectedLi == 'shipment-chart')
    {
        config.options.title.text = "Shipments";
        config.data.datasets = [];
        config.data.datasets.push({
            label: 'Total shipments : ' + d.getFullYear(),
            backgroundColor: '#537B25',
            borderColor: window.chartColors.blue,
            data: [
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
            ],
            fill: false,
        });
    }
    else if (selectedLi == 'customer-chart')
    {
        config.options.title.text = "Customers";
        config.data.datasets = [];
        config.data.datasets.push({
            label: 'Total new customers : ' + d.getFullYear(),
            backgroundColor: '#009954',
            borderColor: window.chartColors.blue,
            data: [
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
                Math.floor((Math.random() * 100) + 1),
            ],
            fill: false,
        });
    }
    window.myLine = new Chart(ctx, config);

    document.getElementById(selectedLi + '-data').onclick = function (evt) {
        var activePoint = myLine.getElementAtEvent(evt)[0];
        var data = activePoint._chart.data;
        var datasetIndex = activePoint._datasetIndex;
        var index = activePoint._index;
        var serviceType = data.datasets[datasetIndex].label;
        var month = data.labels[index];
                if(selectedLi == 'revenue-chart')
            location.href='accounting-report.html?mode=search&month='+month;
        else if(selectedLi == 'shipment-chart')
            location.href='shipment.html?mode=search&month='+month;
        else
            location.href='user.html?mode=search&month='+month;
        
    };
}

/* line chart end */

/* Doughnut chart start */

var randomScalingFactor = function () {
    return Math.round(Math.random() * 100);
};

var dounoughtConfig = {
    type: 'doughnut',
    data: {
        datasets: [{
                data: [
                    Math.floor((Math.random() * 10) + 1),
                    Math.floor((Math.random() * 10) + 1),
                    Math.floor((Math.random() * 10) + 1),
                    Math.floor((Math.random() * 10) + 1),
                    Math.floor((Math.random() * 10) + 1),
                    Math.floor((Math.random() * 10) + 1),
                    Math.floor((Math.random() * 10) + 1),
                    Math.floor((Math.random() * 10) + 1),
                    Math.floor((Math.random() * 10) + 1),
                    Math.floor((Math.random() * 10) + 1),
                ],
                backgroundColor: [
                    "#a41b08",
                    "#f66955",
                    "#537b25",
                    "#00a65a",
                    "#6475e1",
                    "#f39c11",
                    "#303e3f",
                    "#00c0ef",
                    "#9761ad",
                    "#3c8dbc",
                ],
                label: 'Dataset 1'
            }],
        labels: [
            'France',
            'USA',
            'Kenya',
            'Nigeria',
            'Georgia',
            'Canada',
            'Australia',
            'UK',
            'France',
            'Others'

        ]
    },
    options: {
        responsive: true,
        legend: {
            position: 'right',
            onHover: function(e) {
            e.target.style.cursor = 'pointer';
            }
        },
        title: {
            display: false,
            text: 'Chart.js Doughnut Chart'
        },
        animation: {
            animateScale: true,
            animateRotate: true
        },
        hover: {
         onHover: function(e) {
            var point = this.getElementAtEvent(e);
            if (point.length) e.target.style.cursor = 'pointer';
            else e.target.style.cursor = 'default';
         }
      }

    }
};

function customerDounoughtChart() {
    var ctx = document.getElementById('countrywise-customer').getContext('2d');
    dounoughtConfig.data.datasets = [];
    dounoughtConfig.data.datasets.push({
        data: [
            Math.floor((Math.random() * 10) + 1),
            Math.floor((Math.random() * 10) + 1),
            Math.floor((Math.random() * 10) + 1),
            Math.floor((Math.random() * 10) + 1),
            Math.floor((Math.random() * 10) + 1),
            Math.floor((Math.random() * 10) + 1),
            Math.floor((Math.random() * 10) + 1),
            Math.floor((Math.random() * 10) + 1),
            Math.floor((Math.random() * 10) + 1),
            Math.floor((Math.random() * 10) + 1),
        ],
        backgroundColor: [
            "#a41b08",
            "#f66955",
            "#537b25",
            "#00a65a",
            "#6475e1",
            "#f39c11",
            "#303e3f",
            "#00c0ef",
            "#9761ad",
            "#3c8dbc",
        ],
        label: 'Dataset 1'
    });
    window.myDoughnut = new Chart(ctx, dounoughtConfig);

    document.getElementById("countrywise-customer").onclick = function (evt) {
        var activePoints = myDoughnut.getElementsAtEvent(evt);
        var firstPoint = activePoints[0];
        var label = myDoughnut.data.labels[firstPoint._index];
        var value = myDoughnut.data.datasets[firstPoint._datasetIndex].data[firstPoint._index];
        if (firstPoint !== undefined)
            location.href = 'order.html';
//            alert(label + ": " + value);
    };
}
;

/* Doughnut chart end */

/* barchart area for destination country start */

var color = Chart.helpers.color;

function countryDataBarChart() {
    $('#order-by-country').remove();
    $('#order-destination-country').html('<canvas id="order-by-country" style="height:200px"></canvas>');
    var ctx = document.getElementById('order-by-country').getContext('2d');

    window.myBar = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['USA', 'Nigeria', 'Ghana', 'Kenya', 'Australia', 'Canada', 'UK', 'France', 'Georgia', 'Others'],
            datasets: [{
                    label: 'Air Shipment',
                    backgroundColor: '#4b9ece',
                    borderColor: '#4b9ece',
                    borderWidth: 1,
                    data: [
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                    ],
                }, {
                    label: 'Ocean Shipment',
                    backgroundColor: '#76d1da',
                    borderColor: '#76d1da',
                    borderWidth: 1,
                    data: [
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                        Math.floor((Math.random() * 1000) + 1),
                    ],
                }]

        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            legend: {
                position: 'bottom',
                onHover: function(e) {
                        e.target.style.cursor = 'pointer';
                }
            },
            title: {
                display: true,
                text: 'Orders Completed'
            },
            hover: {
             onHover: function(e) {
                var point = this.getElementAtEvent(e);
                if (point.length) e.target.style.cursor = 'pointer';
                else e.target.style.cursor = 'default';
                }
            }
        }
    });

    document.getElementById("order-by-country").onclick = function (evt) {
        var activePoint = myBar.getElementAtEvent(evt)[0];
        var data = activePoint._chart.data;
        var datasetIndex = activePoint._datasetIndex;
        var shipmentType = data.datasets[datasetIndex].label;
        var country = data.labels[datasetIndex];

        if (shipmentType == 'Air Shipment')
            location.href = 'order.html?mode=search&shipmentType=air&country=' + country;
        else
            location.href = 'order.html?mode=search&shipmentType=ocean&country=' + country;
    };

}
;

/* barchart area end */


/* barchart area for service overview start */

var color = Chart.helpers.color;

function overviewBarchart(tabId) {

    $('#Order-overview').remove();
    $('#Shipment-overview').remove();
    $('#Revenue-overview').remove();
    $('.service-overview-div').find('#' + tabId).html('<canvas id="' + tabId + '-overview" height="200px"></canvas>');
    var ctx = document.getElementById(tabId + '-overview').getContext('2d');
    if (tabId == 'Order')
    {
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: chartLabels,
                datasets: [{
                        label: 'Procurement',
                        backgroundColor: '#60b7b1',
                        borderColor: '#60b7b1',
                        borderWidth: 1,
                        data: [
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                        ]
                    }, {
                        label: 'Fill & ship',
                        backgroundColor: '#bcf0ff',
                        borderColor: '#bcf0ff',
                        borderWidth: 1,
                        data: [
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                        ]
                    }, {
                        label: 'Auto',
                        backgroundColor: '#ffdb88',
                        borderColor: '#ffdb88',
                        borderWidth: 1,
                        data: [
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                        ]
                    }]

            },
            options: {
                responsive: true,
                maintainAspectRatio: true,
                legend: {
                    position: 'bottom',
                    onHover: function(e) {
                        e.target.style.cursor = 'pointer';
                    }
                },
                title: {
                    display: true,
                    text: 'Orders Completed'
                },
                onClick: function (c, data) {

                },
                hover: {
                 onHover: function(e) {
                    var point = this.getElementAtEvent(e);
                    if (point.length) e.target.style.cursor = 'pointer';
                    else e.target.style.cursor = 'default';
                  }
               }

            }
        });
        document.getElementById(tabId + '-overview').onclick = function (evt) {
            var activePoint = orderBar.getElementAtEvent(evt)[0];
            var data = activePoint._chart.data;
            var datasetIndex = activePoint._datasetIndex;
            var index = activePoint._index;
            var serviceType = data.datasets[datasetIndex].label;
            var month = data.labels[index];

            if (serviceType == 'Auto')
                location.href = 'auto-shipment.html?month=' + month;
            else if (serviceType == 'Procurement')
                location.href = 'procrument-service.html?month=' + month;
            else
                location.href = 'fill-n-ship.html?month=' + month;
        };

    }
    else if (tabId == 'Shipment')
    {
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: chartLabels,
                datasets: [{
                        label: 'Procurement',
                        backgroundColor: '#60b7b1',
                        borderColor: '#60b7b1',
                        borderWidth: 1,
                        data: [
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                        ]
                    }, {
                        label: 'Fill & ship',
                        backgroundColor: '#bcf0ff',
                        borderColor: '#bcf0ff',
                        borderWidth: 1,
                        data: [
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                        ]
                    }, {
                        label: 'Auto',
                        backgroundColor: '#ffdb88',
                        borderColor: '#ffdb88',
                        borderWidth: 1,
                        data: [
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                        ]
                    }]

            },
            options: {
                responsive: true,
                maintainAspectRatio: true,
                legend: {
                    position: 'bottom',
                    onHover: function(e) {
                        e.target.style.cursor = 'pointer';
                    }
                },
                title: {
                    display: true,
                    text: 'Shipments Completed'
                },
                hover: {
                 onHover: function(e) {
                    var point = this.getElementAtEvent(e);
                    if (point.length) e.target.style.cursor = 'pointer';
                    else e.target.style.cursor = 'default';
                  }
               }
            }
        });

        /*document.getElementById(tabId + '-overview').onclick = function (evt) {
            var activePoint = myBar.getElementAtEvent(evt)[0];
            var data = activePoint._chart.data;
            var datasetIndex = activePoint._datasetIndex;
            var serviceType = data.datasets[datasetIndex].label;
            var month = data.labels[datasetIndex];
            alert(serviceType + month);

        };*/
    }
    else if (tabId == 'Revenue')
    {
        window.myBar = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: chartLabels,
                datasets: [{
                        label: 'Procurement',
                        backgroundColor: '#60b7b1',
                        borderColor: '#60b7b1',
                        borderWidth: 1,
                        data: [
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                        ]
                    }, {
                        label: 'Fill & ship',
                        backgroundColor: '#bcf0ff',
                        borderColor: '#bcf0ff',
                        borderWidth: 1,
                        data: [
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                        ]
                    }, {
                        label: 'Auto',
                        backgroundColor: '#ffdb88',
                        borderColor: '#ffdb88',
                        borderWidth: 1,
                        data: [
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                            Math.floor((Math.random() * 1000) + 1),
                        ]
                    }]

            },
            options: {
                responsive: true,
                maintainAspectRatio: true,
                legend: {
                    position: 'bottom',
                    onHover: function(e) {
                        e.target.style.cursor = 'pointer';
                    }
                },
                title: {
                    display: true,
                    text: 'Revenues Completed'
                },
                hover: {
                 onHover: function(e) {
                    var point = this.getElementAtEvent(e);
                    if (point.length) e.target.style.cursor = 'pointer';
                    else e.target.style.cursor = 'default';
                  }
               }
            }
        });
    }
    
    document.getElementById(tabId + '-overview').onclick = function (evt) {
            var activePoint = myBar.getElementAtEvent(evt)[0];
            var data = activePoint._chart.data;
            var datasetIndex = activePoint._datasetIndex;
            var index = activePoint._index;
            var serviceType = data.datasets[datasetIndex].label;
            var month = data.labels[index];

            if (serviceType == 'Auto')
                location.href = 'auto-shipment.html?mode=search&date=' + month;
            else if (serviceType == 'Procurement')
                location.href = 'procrument-service.html?mode=search&date=' + month;
            else
                location.href = 'fill-n-ship.html?mode=search&date=' + month;
        };

};





/* barchart area end */














