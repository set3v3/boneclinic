$(function () {
  baseUrl = $('#baseUrl').val()

  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass: 'iradio_flat-green',
  })

  var table = $('#example2').DataTable({
    paging: false,
    lengthChange: false,
    searching: false,
    ordering: false,
    columnDefs: [{ orderable: false, targets: -1 }],
    info: false,
    autoWidth: true,
  })

  $('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    // other options
  })

  $('.sortby').on('click', function () {
    var sorttype = 'asc'
    if ($(this).hasClass('sorting_asc')) sorttype = 'desc'

    var sortby = $(this).attr('data-sort')

    $('#field').val(sortby)
    $('#type').val(sorttype)
    $('#frmsearch').submit()
  })

  $('.chk_all').on('ifChecked', function () {
    $('.checkbox').each(function () {
      $(this).attr('checked', 'checked')
      $(this).parent().addClass('checked')
    })
  })
  $('.chk_all').on('ifUnchecked', function () {
    $('.checkbox').each(function () {
      $(this).attr('checked', '')
      $(this).parent().removeClass('checked')
    })
  })
  $('.checkAllCharges').on('ifChecked', function () {
    var cbId = $(this).attr('data-id')
    $('.shipCb_' + cbId).attr('checked', 'checked')
    $('.shipCb_' + cbId)
      .parent()
      .addClass('checked')
  })
  $('.checkAllCharges').on('ifUnchecked', function () {
    var cbId = $(this).attr('data-id')
    $('.shipCb_' + cbId).attr('checked', '')
    $('.shipCb_' + cbId)
      .parent()
      .removeClass('checked')
  })
  $('.checkbox').on('ifUnchecked', function () {
    $('.chk_all').attr('checked', '')
    $('.chk_all').parent().removeClass('checked')
  })

  $('.updateStatusCommon').on('click', function (e) {
    e.preventDefault()

    $('.loading').show()

    var linkElem = $(this)
    var recordEditId = $(this).attr('data-edit')
    var baseUrl = $('#baseUrl').val()
    var recordStatus = $(this).attr('data-status')
    var ajaxMethod = $('#ajaxPage').val()
    var postStatus = 1
    if (recordStatus == 1) postStatus = 0

    $.ajax({
      type: 'get',
      dataType: 'json',
      url: baseUrl + '/' + ajaxMethod + '/' + recordEditId + '/' + postStatus,
      data: {},
      success: function (msg) {
        $('.loading').hide()
        if (msg.updated) {
          //console.log('i am here');
          if (recordStatus == 1) {
            linkElem.removeClass('btn-success')
            linkElem.addClass('btn-danger')
            linkElem.attr('data-status', 0)
            linkElem.text('Inactive')
          } else {
            linkElem.removeClass('btn-danger')
            linkElem.addClass('btn-success')
            linkElem.attr('data-status', 1)
            linkElem.text('Active')
          }
        }
      },
      error: function () {
        $('.loading').hide()
      },
    })
  });

  $('.blockUnblockUser').on('click', function (e) {
    e.preventDefault()

    $('.loading').show()

    var linkElem = $(this)
    var recordEditId = $(this).attr('data-edit')
    var baseUrl = $('#baseUrl').val()
    var recordStatus = $(this).attr('data-status')
    var ajaxMethod = $('#ajaxPage').val()
    var postStatus = 2
    if (recordStatus == 2) postStatus = 1

    $.ajax({
      type: 'get',
      dataType: 'json',
      url: baseUrl + '/' + ajaxMethod + '/' + recordEditId + '/' + recordStatus,
      data: {},
      success: function (msg) {
        $('.loading').hide()
        if (msg.updated) {
          //console.log('i am here');
          if (recordStatus == 1) {
            linkElem.removeClass('btn-success')
            linkElem.addClass('btn-danger')
            linkElem.attr('data-status', 2)
            linkElem.text('Disable Account')
            linkElem.prev().text('Active')
          } else {
            linkElem.removeClass('btn-danger')
            linkElem.addClass('btn-success')
            linkElem.attr('data-status', 1)
            linkElem.text('Enable Account')
            linkElem.prev().text('Disabled')
          }
        }
      },
      error: function () {
        $('.loading').hide()
      },
    })
  });

  $('#table-search').keyup(function (e) {
    $('#searchData').val($(this).val())
    if (e.keyCode == 13) {
      $('#frmsearch').submit()
    }
  })

  $('#resetFilter').on('click', function () {
    $('#searchData').val('')
    $('#frmsearch').submit()
  })

  $('.editStatus').on('click', function () {
    var recordEdit = $(this).attr('data-id')
    var recordStatus = $(this).attr('data-status')
    $('#recordStatus option[value="' + recordStatus + '"]').attr(
      'selected',
      true,
    )
    //console.log(recordEdit);
    var formAction = $('#form-action').val()
    formAction = formAction + '/' + recordEdit
    $('#editStatus-form').attr('action', formAction)
  })

  $('.checkbox-update').on('ifUnchecked', function () {
    //console.log($(this).attr('class'));
    $(this).parent().next().val('0')
  })
  $('.checkbox-update').on('ifChecked', function () {
    //console.log($(this).attr('class'));
    $(this).parent().next().val('1')
  })

  $(document).on('blur', '.numFormat', function () {
    if ($.isNumeric(this.value)) {
      var newVal = parseFloat(this.value)
      $(this).val(newVal.toFixed(2))
    } else {
      var newVal = 0
      $(this).val(newVal.toFixed(2))
    }
  })
  $(document).on('blur', '.intFormat', function () {
    if ($.isNumeric(this.value)) {
      var newVal = parseInt(this.value)
      $(this).val(newVal)
    } else {
      //var newVal = 0;
      $(this).val('')
    }
  })

  //Collapse Box
  $('.collapse')
    .on('shown.bs.collapse', function () {
      $('.accordion-toggle')
        .find('.fa-plus')
        .removeClass('fa-plus')
        .addClass('fa-minus')
    })
    .on('hidden.bs.collapse', function () {
      $('.accordion-toggle')
        .find('.fa-minus')
        .removeClass('fa-minus')
        .addClass('fa-plus')
    })

  $('.customRange').on('ifChecked', function () {
    document.getElementById('customDaterange').style.visibility = 'visible'
  })
  $('.customRange').on('ifUnchecked', function () {
    document.getElementById('customDaterange').style.visibility = 'hidden'
  })
})

function showAddEdit(id, page, urlSegment) {
  var url = urlSegment + '/' + id + '/' + page
  $.ajax({
    method: 'GET',
    url: baseUrl + '/' + url,
  }).done(function (response) {
    $('#modal-addEdit').html(response)
    $('#modal-addEdit').modal({ backdrop: 'static', keyboard: false })
    $('#modal-addEdit').modal('show')
  })
}

function showModal(urlSegment) {
  var url = urlSegment
  alert(baseUrl)
  $.ajax({
    method: 'GET',
    url: baseUrl + '/' + url,
  }).done(function (response) {
    $('#modal-export').html(response)
    $('#modal-export').modal({ backdrop: 'static', keyboard: false })
    $('#modal-export').modal('show')
  })
}

function isNumberKey(evt, obj) {
  var charCode = evt.which ? evt.which : event.keyCode
  var value = obj.value
  var dotcontains = value.indexOf('.') != -1
  if (dotcontains) if (charCode == 46) return false
  if (charCode == 46) return true
  if (charCode > 31 && (charCode < 48 || charCode > 57)) return false
  return true
}

function deleteSelected() {
  if ($('.checkbox:checked').length == 0) {
    alert('Please select atleast one item!!')
  } else {
    $('#modal-default').modal({ backdrop: 'static', keyboard: false })
    $('#modal-default').modal('show')
  }
}

function updateAction(action) {
  if ($('.checkbox:checked').length == 0) {
    alert('Please select atleast one item!!')
  } else {
    if (action == 'delete') {
      var r = confirm(
        'All the selected items will be deleted. This action cannot be reversed back!!',
      )

      if (r == true) {
        $('#action').val(action)
        $('#frmedit').submit()
      }
    } else if (action == 'update') {
      $('#action').val(action)
      $('#frmedit').submit()
    }
  }
}

/* THIS IS FOR LANDING PAGE AND STATIC PAGE SEARCH FUNCTIONALITY */
$(document).ready(function () {
  $('#table-search-button-pages').click(function () {
    $('#searchData').val($('#table-search-text').val())
    $('#frmsearch').submit()
  })
})
