<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticate(Request $request)
    {


        $user = DB::table('users')
                    ->where('users.email', '=', $request->input('email'))
                    ->where('users.user_type', '=', 0)
                    ->first();

        //print_r($request->input('email'));
        //die('here');


        if ($user !== null) {
            if ($user->is_verified === 1) {
                if ($user->active_status === 1) {
                    if (!Auth::attempt($request->only('email', 'password'))) {
                        return redirect()->route('login')->with('errorMessage', 'Email and password are not correct.');
                    }
                } else {
                    return redirect()->route('login')->with('errorMessage', 'Account is not active yet! Please contact admin.');
                }
            } else {
                return redirect()->route('login')->with('errorMessage', 'Account is not verified! Please contact admin.');
            }
        } else {
            return redirect()->route('login')->with('errorMessage', 'Account does not exist.');
        }

        return redirect()->intended('dashboard');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}