<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use App\Models\User;
use App\Models\Franchisee;
use App\Models\Order;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    // Function for Retrieving the Dashboard Parameters

    public function index()
    {
        $data =[];
        //$data['customers'] = User::getCustomerCount();
        //$data['restaurants'] = Restaurant::getRestaurantCount();
        //$data['franchisees'] = Franchisee::getFranchiseeCount();
        //$data['orders'] = Order::getCompletedOrderCount();

        $param['status'] = 'current';
        $param['field'] = 'created_at';
        $param['type'] = 'desc';
        $param['searchDisplay'] = 15;

        //$data['orderList'] = Order::getOrderList($param);

        return view('dashboard.index', compact('data'));
    }
}
