<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Emailtemplate extends Model
{
    public $timestamps = true;


    /**
     * @var $fillable
     */
    protected $fillable = [
        'template_subject',
        'template_body',
    ];

    public function __construct()
    {
        parent::__construct();
    }

    public function getData($param)
    {
        $where = '1 = 1';

        if (!empty($param['searchData'])) {
            $where .=
                "  AND (template_key like '%" .
                addslashes($param['searchData']) .
                "%' OR template_subject like '%" .
                addslashes($param['searchData']) .
                "%') ";
        }

        $resultSet = Emailtemplate::whereRaw($where)
            ->orderBy($param['sortField'], $param['sortOrder'])
            ->paginate($param['searchDisplay']);

        return $resultSet;
    }
}
