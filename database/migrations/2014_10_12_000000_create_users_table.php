<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('user_type')->default('0')->nullable();
            $table->tinyInteger('active_status')->default('0')->comment('0=>Inactive,1=>Active');
            $table->tinyInteger('is_verified')->default('0')->comment('0=>Not Verified,1=>Verified');
            $table->string('email')->unique()->notNullable();;
            $table->string('password')->notNullable();;
            $table->timestamp('email_verified_at')->nullable();
            $table->string('active_token')->nullable()->comment('Account activation token');
            $table->string('password_reset_token')->nullable()->comment('Password reset token');
            $table->string('vp')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        // Insert some stuff
        DB::table('users')->insert(
            array(
                'user_type' => 0,
                'active_status' => 1,
                'is_verified' => 1,
                'email' => 'admin@admin.com',
                'password' => '$2y$10$3G.oL/ZN/PyL.6UmnDsgc.ghdF8p0/kNbH2dq3Fi4hkIqo9J9VtOC',
                'vp' => '123',
            )
        ); 


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
