@extends ('layouts.master')

@section('title', 'Add Customer')

@section('content')
<section class="content">
    <div class="row"> 
        <section class="col-lg-10 connectedSortable"> 
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
            <form id="addeditFrm" action="{{ route("users.store") }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>First Name <span class="text-red">*</span></label>
                                <input id="first_name" name="first_name" required class="form-control" type="text" value="{{ old('first_name') }}" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Last Name <span class="text-red">*</span></label>
                                <input id="last_name" name="last_name" required class="form-control" type="text" value="{{ old('last_name') }}" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email Address <span class="text-red">*</span></label>
                                <input id="email" name="email" required class="form-control" value="{{ old('email') }}" type="email" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Phone <span class="text-red">*</span></label>
                                <input id="phone" name="phone" required class="form-control" value="{{ old('phone') }}" type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password <span class="text-red">*</span></label>
                                <input id="password" name="password" required class="form-control"  type="password" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Confirm Password <span class="text-red">*</span></label>
                                <input id="confirm_password" name="confirm_password" class="form-control"  type="password" />
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-right">
                    <a class="btn btn-info" href="{{ route('users.index') }}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                    <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                </div>
               </form>
                <!-- /.box-body -->
            </div>
            <!-- /.nav-tabs-custom --> 
        </section>
    </div>
    <!--Block 01-->
</section>

<script>
$(function () {
    $("#addeditFrm").validate({
        rules: {
            confirm_password: {
                equalTo: "#password"
            }
        }
    });
});
</script>

@endsection
