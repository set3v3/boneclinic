@extends('layouts.master')

@section('title', 'Customers')

@section('content')

<script src="{{ asset('controller-css-js/customer.js') }}"></script>

<section class="content" id="mainContentNotLoad">
    <div class="row m-b-15">
        <div class="col-lg-9 col-md-9">
            <form method="POST" action="{{route("users.index")}}" name="frmsearch" id="frmsearch">
                @csrf
                <label>Entries per page</label>
                <div class="adddrop m-l-15 w-100">
                    <select name="searchDisplay" id="searchDisplay" class="form-control highLight"
                        onchange="$('#frmsearch').submit();">
                        <option {{$searchData['searchDisplay']=='10'?'selected':''}} value="10">10</option>
                        <option {{$searchData['searchDisplay']=='20'?'selected':''}} value="20">20</option>
                        <option {{$searchData['searchDisplay']=='30'?'selected':''}} value="30">30</option>
                        <option {{$searchData['searchDisplay']=='40'?'selected':''}} value="40">40</option>
                        <option {{$searchData['searchDisplay']=='50'?'selected':''}} value="50">50</option>
                    </select>
                </div>
                <input type="hidden" name="field" id="field" value="{{$searchData['field']}}" />
                <input type="hidden" name="type" id="type" value="{{$searchData['type']}}" />
            </form>
        </div>
        <div class="col-lg-3 col-md-9 text-right">

            <div class="btn-group actionGroup">
                <a class="btn btn-info btn-sm" href="{{ url('users/create') }}"><span class="Cicon"><i
                            class="fa fa-plus"></i></span> Add New</a>
            </div>
            <a class="accordion-toggle btn-sm btn btn-success" data-toggle="collapse"
                href="#collapseTwo"><span class="Cicon"> <i class="fa fa-minus"></i></span> Advanced Search </a>
        </div>
    </div>
    <div class="col-md-12 m-t-15">
        <div class="row">
            <div id="collapseTwo" class="panel-collapse in collapse">
                <div class="box">
                    <div class="box-body">
                        <form method="POST" action="{{route("users.index")}}" name="frmsearch" id="frmsearch">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label>Date Period</label>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <!-- <label for="inputEmail4">Email</label>-->
                                    <div class="withRdaioButtons" style="padding:15px 0 0 0;">
                                        <label>
                                            <input @isset($searchData['search_rangedate'])
                                                {{$searchData['search_rangedate']=='all'?'checked':''}} @endisset
                                                type="radio" name="search_rangedate" value="all" class="flat-red"
                                                onclick="show1();" checked>
                                            <span class="radioSpan">All dates</span> </label>
                                        <label>
                                            <input @isset($searchData['search_rangedate'])
                                                {{$searchData['search_rangedate']=='thismonth'?'checked':''}} @endisset
                                                type="radio" name="search_rangedate" value="thismonth" class="flat-red"
                                                onclick="show1();">
                                            <span class="radioSpan">This month</span> </label>
                                        <label>
                                            <input @isset($searchData['search_rangedate'])
                                                {{$searchData['search_rangedate']=='thisweek'?'checked':''}} @endisset
                                                type="radio" name="search_rangedate" value="thisweek" class="flat-red"
                                                onclick="show1();">
                                            <span class="radioSpan">This week</span> </label>
                                        <label>
                                            <input @isset($searchData['search_rangedate'])
                                                {{$searchData['search_rangedate']=='today'?'checked':''}} @endisset
                                                type="radio" name="search_rangedate" value="today" class="flat-red"
                                                onclick="show1();">
                                            <span class="radioSpan">Today</span> </label>
                                        <label>
                                            <input @isset($searchData['search_rangedate'])
                                                {{$searchData['search_rangedate']=='custom'?'checked':''}} @endisset
                                                type="radio" name="search_rangedate" value="custom"
                                                class="flat-red customRange" onclick="show2();">
                                            <span class="radioSpan">Custom</span> </label>
                                    </div>
                                </div>
                                @isset($searchData['search_rangedate'])
                                @if($searchData['search_rangedate']=='custom')
                                <input type="hidden" value="custom" name="isCustomSelected" id="isCustomSelected">
                                @endif
                                @endisset
                                <div class="form-group col-md-6">
                                    <div id="customDaterange" class="form-group dateRange">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon input-daterange"> <i
                                                        class="fa fa-calendar"></i> </div>
                                                <input type="text" class="form-control pull-right" name="search_date"
                                                    id="search_date">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-2">
                                    <label for="sel1">First Name</label>
                                    <input class="form-control" placeholder=""
                                        value="@isset($searchData['search_firstName']) {{$searchData['search_firstName']}} @endisset"
                                        type="text" name="search_firstName">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="sel1">Last Name</label>
                                    <input class="form-control" placeholder=""
                                        value="@isset($searchData['search_lastName']) {{$searchData['search_lastName']}} @endisset"
                                        type="text" name="search_lastName">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="sel1">Email Address</label>
                                    <input class="form-control" placeholder="" type="text"
                                        value="@isset($searchData['search_email']) {{$searchData['search_email']}} @endisset"
                                        name="search_email">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="sel1">Phone</label>
                                    <input class="form-control" placeholder="" type="text"
                                        value="@isset($searchData['search_phone']) {{$searchData['search_phone']}} @endisset"
                                        name="search_phone">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="sel1">Status</label>
                                    <select class="form-control customSelect2" id="sel1" name="search_status">
                                        <option>All</option>
                                        <option @isset($searchData['search_status'])
                                            {{$searchData['search_status']=='1'?'selected':''}} @endisset value="1">
                                            Active</option>
                                        <option @isset($searchData['search_status'])
                                            {{$searchData['search_status']=='0'?'selected':''}} @endisset value="0">
                                            Inactive</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-12 text-right">
                                    <button type="submit" class="btn btn-success"><span class="Cicon"><i
                                                class="fa fa-search"></i></span>Search</button>
                                    <button type="button" onclick="location.href ='{{url('users/showall')}}'"
                                        class="btn btn-danger"><span class="Cicon"><i
                                                class="fa fa-refresh"></i></span>Show All</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <section class="col-lg-12 connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="box">
                <input type="hidden" id="ajaxPage" value="users/updatestatus">
                <div class="box-body">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th data-sort="first_name" class="{{$sort['first_name']['current']}} sortby">Name</th>
                                <th data-sort="email" class="{{$sort['email']['current']}} sortby">Email Address</th>
                                <th data-sort="phone" class="{{$sort['phone']['current']}} sortby">Phone</th>
                                <th data-sort="created_at" class="{{$sort['created_at']['current']}} sortby">Registered
                                    On</th>
                                <th class="text-center" width="15%">Status</th>
                                <th width="15%">Actions</th>
                            </tr>
                        </thead>
                        <tbody id="checkboxes">
                            @if(!empty($customerData))
                            @foreach ($customerData as $customer)
                            <tr>
                                <td>{{$customer->first_name}} {{$customer->last_name}}</td>
                                <td>{{$customer->email}}</td>
                                <td>{{$customer->phone}}</td>
                                <td>{{$customer->created_at->format('j F, Y h:i A')}}</td>
                                <td class="text-center">
                                    @if($customer->action==1)
                                    <span>Active</span>&nbsp;
                                    <a data-toggle="confirmation" title=""
                                        class="btn btn-danger btnActive blockUnblockUser"
                                        data-edit="{{ $customer->id }}" data-status="2"
                                        data-original-title="Click to Disable Account">Disable Account</a>
                                    @else
                                    <span>Disabled</span>&nbsp;
                                    <a data-toggle="confirmation" title=""
                                        class="btn btn-success btnActive blockUnblockUser"
                                        data-edit="{{ $customer->id }}" data-status="1"
                                        data-original-title="Click to Enable">Enable Account</a>
                                    @endif
                                </td>
                                <td>
                                    <a class="text-green actionIcons" href="{{ route('users.show', $customer->id) }}"><i
                                            data-toggle="tooltip" title="Click to View" class="fa fa-fw fa-eye"></i></a>
                                    <a class="text-green actionIcons" href="{{ route('users.edit', $customer->id) }}"><i
                                            data-toggle="tooltip" title="Click to Edit"
                                            class="fa fa-fw fa-edit"></i></a>
                                    <a class="color-theme-2 actionIcons"
                                        href="{{ route('users.destroy', $customer->id) }}" data-toggle="confirmation"><i
                                            data-toggle="tooltip" title="Click to Delete"
                                            class="fa fa-fw fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8">No Record Found</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="row mt-20">
                    <div class="col-md-6 col-sm-6 col-xs-6 for12">
                        <p class="results">Showing
                            {{ $customerData->firstItem() . ' - ' . $customerData->lastItem() . ' of  ' . $customerData->total() }}
                        </p>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-6 for12 text-right">
                        <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">
                            {{ $customerData->links() }}
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.nav-tabs-custom -->
        </section>
    </div>
</section>
<!-- /.content -->
@endsection