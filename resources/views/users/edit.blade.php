@extends ('layouts.master')
@section('title', 'Edit Customer')
@section('content')
<section class="content">
        <div class="row"> 
            <section class="col-lg-10 connectedSortable"> 
                <!-- Custom tabs (Charts with tabs)-->
                <div class="box">
                    <form action="{{ route('users.update', [$user->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>First Name <span class="text-red">*</span></label>
                                        <input id="first_name" name="first_name" required class="form-control" type="text" value="{{ old('first_name', isset($user->userDetail) ? $user->userDetail->first_name : '') }}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Last Name <span class="text-red">*</span></label>
                                        <input id="last_name" name="last_name" required class="form-control" type="text" value="{{ old('last_name', isset($user->userDetail) ? $user->userDetail->last_name : '') }}" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email Address <span class="text-red">*</span></label>
                                        <input id="email" name="email" required class="form-control" value="{{ old('email', isset($user) ? $user->email : '') }}" type="email" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone <span class="text-red">*</span></label>
                                        <input id="phone" name="phone" required class="form-control" value="{{ old('phone', isset($user) ? $user->phone : '') }}"}" type="text" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer text-right">
                            <a class="btn btn-info" href="{{ route('users.index') }}"><span class="Cicon"><i class="fa fa-arrow-left"></i></span>Back</a>
                            <button type="submit" class="btn btn-success"><span class="Cicon"><i class="fa fa-paper-plane"></i></span>Save</button>
                        </div>
                    </form>
                    <!-- /.box-body -->
                </div>
                </section>
            </div>
    </div>
    <!--Block 01-->
</section>
<script>
$(function () {
$("#addeditFrm").validate();

});
</script>
@endsection

