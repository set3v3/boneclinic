@extends('layouts.master')

@section('title', 'View Customer')

@section('content')
<section class="content">
    <ul class="nav nav-tabs" role="tablist">
        <li class="active">
            <a href="#account" role="tab" data-toggle="tab">
                <icon class="fa fa-user"></icon> Account
            </a>
        </li>
        <li><a href="#address" role="tab" data-toggle="tab">
                <i class="fa fa-home"></i> Addresses
            </a>
        </li>
        <li>
            <a href="#order" role="tab" data-toggle="tab">
                <i class="fa fa-cart"></i> Orders
            </a>
        </li>
        <li>
            <a href="#items" role="tab" data-toggle="tab">
                <i class="fa fa-pencil"></i> Items
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane fade active in" id="account">
            <div class="box">
                <div class="box-body">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th>
                                    Name
                                </th>
                                <td>
                                    {{ $user->userDetail->first_name." ".$user->userDetail->last_name }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Email
                                </th>
                                <td>
                                    {{ $user->email }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Phone
                                </th>
                                <td>
                                    {{ $user->phone }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="box-footer text-right">
                    <a class="btn btn-info" href="{{ route('users.index') }}"><span class="Cicon"><i
                                class="fa fa-arrow-left"></i></span>Back</a>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="address">
            <div class="box">
                <div class="box-body">Coming soon</div>
            </div>
        </div>
        <div class="tab-pane fade" id="order">
            <div class="box">
                <div class="box-body">Coming soon</div>
            </div>
        </div>
        <div class="tab-pane fade" id="items">
            <div class="box">
                <div class="box-body">Coming soon</div>
            </div>
        </div>
    </div>
</section>
@endsection