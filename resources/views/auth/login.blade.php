@extends('layouts.login')
@section('content')

@php
if(!empty($_COOKIE['shoptomydoor_adminUser_username'])){
$var1 = $_COOKIE['shoptomydoor_adminUser_username'];
$var2 = $_COOKIE['shoptomydoor_adminUser_password'];
$var3 = 'checked';

} else {
$var1 = '';
$var2 = '';
$var3 = '';
}
@endphp

<div class="login-box">
  <div class="login-logo"> <a href=""><img src="{{ asset('img/logo.png') }}" /></a> </div>
  @include('elements.notification')
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
    <form role="form" method="POST" action="{{ route('logged_in') }}" id='loginForm' novalidate="novalidate">
      {{ csrf_field() }}
      <div class="form-group has-feedback">
          <!-- <input type="email" id="email" required="" class="form-control" placeholder="Email"> -->
          <input type="text" class="form-control validate[required,custom[email]]" name='email' required placeholder="Email" value="{{$var1}}" autofocus/>
          {{ $errors->first('email') }}


        <span class="help-block-email" style="display: none" >Help block with error</span>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span> </div>
      <div class="form-group has-feedback">
          <!-- <input type="password" id="password" required="" class="form-control" placeholder="Password"> -->
          <input type="password" class="form-control validate[required]" name='password' required placeholder="Password" value="{{$var2}}"  />
          {{ $errors->first('password') }}
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember" id="remember" value="{{$var3}}" {{$var3}} class="flat-red">
              Remember me </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
            <!-- <input type="submit" class="btn btn-primary btn-block btn-flat" value="Sign In" /> -->
            <button class="btn btn-primary btn-block btn-flat" type="submit">Sign In</button>
        </div>
        <!-- /.col --> 
      </div>
    </form>
    <!-- /.social-auth-links --> 
    <a href="{{ url('/password/reset') }}">I forgot my password</a><br>
    </div>
  <!-- /.login-box-body --> 
</div>
<!-- /.login-box --> 
 
@endsection