<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="">
    <meta name="viewport" content="">

      <title>TheDelhiveryGuys Admin Panel</title>
        <script src="{{ asset('/global/bower_components/jquery/dist/jquery.min.js') }} "></script> 
        <!-- Bootstrap -->
        <link href="{{ asset('/global/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{ asset('/global/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
        <!-- Ionicons -->
        
        <!-- Theme style -->
        <link href="{{ asset('/global/bower_components/css/skins/_all-skins.min.css') }}" rel="stylesheet">
        <link href="{{ asset('/global/bower_components/css/AdminLTE.min.css') }}" rel="stylesheet">
        <!-- iCheck -->
        <link rel="stylesheet" href="/assets/vendors/font-awesome/css/blue.css">
        <link href="{{ asset('/global/bower_components/iCheck/skins/flat/green.css') }}" rel="stylesheet">
        <link href="{{ asset('/global/vendors/css/skins/style.css') }}" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
         
  </head>

  <body class="hold-transition login-page">
    <!-- <div> -->
      

      <!-- <div class="login_wrapper"> -->
        @yield('content')
      <!-- </div> -->
    <!-- </div> -->

    <!-- jQuery 3 --> 
    
    <!-- Bootstrap 3.3.7 --> 
    
    <!-- iCheck 1.0.1 --> 
    <script src="{{ asset('/global/bower_components/iCheck/icheck.min.js') }}"></script> 

    <script>
        //Flat red color scheme for iCheck
      $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_flat-green',
        radioClass   : 'iradio_flat-green'
      })
    </script>



  </body>
</html>
