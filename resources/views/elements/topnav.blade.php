<header class="main-header">
    <!-- Logo -->
    <a href="javascript:void(0);" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAABGCAYAAAC+ANZHAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABRZJREFUeNrMWk1o1EAUnk2LrWg1hwr+1eYgiHroVsFDFVxR/DkUU/Wil25FPVjQVZGexBU8iAptBXG1YvcieOu2KigeusUfqKBNQSx4aav4c1C61r+tbY0zdSKzszOZJJNs98GDTTKZ/ebNzHvfe5OQaZrAB1Gh1kANQ81AHYRq+NFxqeT7jVCjUCOc5ymo7VDTxD0NajUejIoH0ocHli/Igh40AnXYdC6tUBsF73ThNjn/5QVcqxms9EJVvQLs5HRaxWhbJQFy2ALpBpzuEBitHUT7MReDaUPPQja7GC3m3Xgxa8SitiTkYjPtgPrQQTsaTC3PUqINUOVxczlRUmKkBZF1umxcBvBoPbcyRsxUN4l8gGOtp3gdWWtpR4DWA7j//2I56jheYyIrHQYFFjTFKjZroaZQJE+h1uHffQoOVcUCDhDgkGQQQJ28McvgqvJiOXaeZoE2gJ3Szjozs/yom7MFroPhPXQr1I3MggVFcbrNaqtQxPJBgdbaGs79r1CboMZIPhgtYBjjhTTLaiqLD6rUNEuvxWzP9fHvF48udgFQ57VTsGuJMlhFh9f5m3rzsiJU8uejdT1x98Z4tvvaHZtXIqKcBOUM56CeJZ4dwuqaLEyPDgHz8wfwK9FiTr3uB38+vweltdtjVLNnhFMO24U68jqG4/JCgZFOQm21Ln5cal48/e7Vx5JF//zs1FB/rhXWbwXzTiVCDKf8VjRoFmHVMEhdADSnw2zXlaaJe523wK/veQ2VymWgbNuBzPSPsfq5+1uecAhqLStVDQnyYotFW4z6uN2IJ3quv8jeubyOO6LKpWDOlr0HyxuOdTIAIveSdJsXG9RCPs5riCz4+36SCa5k9QZQurbufPme5jOCjZIHUHGxOdO8wI7ATfY/uqVUr54Bkyc/vwEOuJvE7xqna1AEcjOxCzeyGo0f22yiXVy+//S3SaOvYnroOdrFS+advvpJkCiFZCxIT3kdr5GyaDmYs7MRlNUfWTD/zO1Q2b7Y+ZKVq3Y56D8iW/rQnUScbHfihdcsjn7udorRbh72mX2TALopAu16ikegjhLXJySAJRiJeo0f1a2UB2KBeOZjh3UZlSYLbiXtwjqWIp65ycF7iLiskLVghBoxss4XDxWs35z7AzLlNx7ZFAlKgNJQ43iA5DSG8TO6RigFMC0AZEBNYrYedtgnzezjMgDbGBZiWUdmA45ZaacXRVa5QHSW9ilPUfFgLYkqHn2YEVAGmKEYja6A4pMU8bu6GAHmEOZiBzgoc9KE1uGWAKpiZKE+Uyq5oNMBWI3M0Q23dCtooau9DUqRgeslrhGtSxULQA2DC1NFBOnjWM/uAxcFNMygdep5u+UPgwAYIY7Q6OMzJ9JO1wf90jCmSV4lhclGTr9+WbA1Z9Ri6SNyHANP5wg7pZK3XCenWipLvaQIKy9PRrRL87NULOOo0eIfwDtxJm7iDeLrYZAiuVs1ojqvB3FSJQOQ9F1J7iKfRYAaVXEIRPxyM3ZfGcUZJQ6e60kHBdBOzjpsd44FUGaKM1RsLbopNnDMtXZ0m41leNICtZzBpH2JJGG7qpSHGk+cdxQmY0GyVtjrgrmogDgICsrNAIoghJ3+KW7nbN36EC+TjA/DGm1iN/lVU1Y0xX4F9aTNp3a9HJ6IajAJEUC//GAUR5MYyD3fi3Daj3Kovu9rkI4YGnYro7xKAfh3JqcBh9+4Bp0XRwinbnDiuUbE87yY/leAAQDtrVogDZxWiAAAAABJRU5ErkJggg==" height="30" width="30" /></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJsAAABGCAYAAADbweZ9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAADw9JREFUeNrsXXtwVsUV35BAoxj5oI2DcdCUklKpNhmshSKW8JhCKZpg/0CsIwkDg2DlYaUMHZTwKA7CkEAL8hoILQN0piMvYWCgEuShiGJSBXmMIcBUGKjwhfAICeHrnuRc2Wx29+7ex/fdxO83cyZf9n3vPXf3nLNnz02IRCLEA4QoZVLKohSmVEaplMQRB4NWLuuPoLSH0hVKJZSKKBVT+owScPFGStlcnXRKfShNoDSdUg4yqxu8iP1ZdJXSJPx9nNJcSj0U9TOxDtvGUkofc2lAXbEOn7eNUqKij6Nc+b9i+gDB2EVYwJWD9pIp5XPpFylNE4y7BMu3pnSeyxtL6T+UblLaQWk4luWxjqs3SlBmjqDv/PocmNkcUDal0xF9FFIaYVNnI5ZxMp6LBmOZKqg/w6D+eKwjQkfFGEWA9AWC9H6a9RMp1RiMPYNSqkH5EkppGtdgUYqkDNxfRzNbIc5m6QZ1JuKMp6qTi2X2eDDTqQBv3r4ArS5HBGkvcf+3ldSt83lssAL9l1IG/v8vm/Lfl6TPdLKMrkbG4fEwpQSOHnZ4gdm4DPvJcL1RBHCCyx6P5ZBEPOEfOo8KB31VOxzjAQWzZTC/O6heChNmg5knT8Jk5wTlz2HeSiYtrMmYMAMWuHyIyyj9mNJoSX6xRhttKN3H0VqPme2URpm+grRXbSaFBwSkYran8RnvF+SlUkqhtF2Qt4T5/aQgf6f1I0nReToK71n4O4vLT9C8maPxjdipKGMxJqsaT0CGcKrV3sAHeQr7vyLRosOKNmqRYoHWTN/9BPnvKerWOOjPYrLN3HOw0J3SXkH6AOb3UEH+GpU2Ctx9GqkIZ7NsblkzXSJ3apZLECypXkDGUJ0CIrcdE6R15h50NPGWIM3S5udLZj7AQJWY0Ip7y/egucJO+D/n44WGfWA2GVICwmyLJMsakZgg/MYFhfC/WJD3nKKtchGz7ZE83IMod1my1yCfL/R55ndOFJaqIEA08w9lJgGhducjRAx+XaGYzJHYGD9n/7FktgJNmWw0aVlIDMg4RA9wsELDe9/n8YiWwzOcApLPaaFd7JbjVvjmTHco/PuBN5nfewPCkLFkSpFI87WPL1FniUJy0GbSmS1I28IzW16AGA3QS0OwjxZAK7uNtMLnvv4pkSmHOGC2YaRh28qiDhr938br/UrDRCMyJucolt5vmS03QA+X1w43BWjJHeVz+28I0jK45Ur4EDUUn24uRQon4tNkPgGYLVMinMeC0c4y/1cGjNn8xikJsyUrtPVoyKZ5pLFh3sJaG2VrvojZQjZaUTSwgmM06yLDAWKGgzHo8zFB2kYH7ZzRKHNdwlBrJOVXmw6iFTeQgVGcxVSuKgsDMqvdT+5u9WTHWH618K5GvTWk8TZVhUadBwVpLyrKqzTitTJmY7eDdkTpJspkiEqUUSYGZDaronQJqTZK/bH4iaDMfo12qh32LVpJ2irqhA2UnXpm22QjpPsB0XK9ENX8YvLdBX9f0gwesBd4R5BWoCj/siS9TMVs7FJ61u2Ib21dfvX6vFc6GlQZirNZtGW0uoAx2+4Y979ckPa6orzIDgrbbOdkzBYmTW1tEeLCrnT75JGUhMQ751nmq978zgZFlewY3dzagDFbiU3+n33uXybbdTBo4yGVgmBd5AwubxRp6ktukXq6OPMlqf1kN7m5dEqkanx2pHr9vJS6k19MVGh3WTF6uOGAMdsJm/xFMXoBhxnUnyTLSOLW5jD+bafRYOG3OjNdMuvOfXE+MbVB3Iv8r8HAXfNBg+KU9ER/0vaPi3lPgueZJbtPjB7u16R54XoU+uhJ6VMubYxEnhOhh93MZqEIZ5k1qBnKsKCRujJ58YU2fX838nbFUXL7y6ZeznfOHCc1W5Zfubl+bm8mmV/XvZ7d7kWtFxj5gMOZLRWVlk5I6Tbl05iyFpkaVL3YD26LlIzU1qCu6ExEphcPJMHm3CgwQIjc9dSdwNZtohhsWfZp9Yb5Uke/hB+kEWDK5KHjVzOyoYV8F5roRXLXgY9dDlQuRBOYZQlEiDc1+/otaXCP1j1wuwy1NlF50T50DhHbGLdj3zxqiL6r1APM/VKNRTTW9oKXE5TA85rXpXQLB5RyQvwEqWFn46L8mm3FQkZLfPQXJOmnvWYnP/fKGzZKgpdmD9UDKHch/3QxLD/YsLzs5NdGD+6JnRs82xfv4g0rxGY3nZsceOE1pU4so9Ue2rWq1SOP1jNWE9yoIhJGW+nRVG1iwgCHvh+56OuOzxqv7PSW7GBMDfEeol2KFa5NR4aHgUuYg6cHZOUqX/1VJDy8S6R6y7KrVTNfqP997e1xHTUO7zo9ND1WcDB2FvP7NB4GzpTU7yGov4nSWUG61Qaf9zF3fyyMwvJXuPQNiuuBQ8r7mLJf4QFgUdnxgj4XCtKOU2qLB5t5/IlrE57VVa7MVkHfrQVtLZFdV4JhrI8iO7kNcG3W7yOJdJa756Vp9fnV7y6eRRLqyhlZjUjkg74atqY4milMmS2Xkx2EzAaKwveeHfOE7uTKmVSK4o8lzmwEtdLTdsxmCHYAm0ljZ844WhBMwy9UkMb7qJNc9L1UoGJnxh9JfGZjsYk09jfXmd3ATw7COPXWKNueBG8bKY4YzGxEU4C3Zi2LdmgymhULJI74zFaPbNJwoNkCHFqGIHEdDNuRWfhLUSuNz25xZmsi1OugEpmoBKmUYaYs1ED7cLNnX0PFxZIpdQCnj2Af84ThdcAe468pPYOiAexjHkcRAdyk30Mlxy48RbJEPgWP4HJJHdhjhU1u3pAKXrllmN9dsFqdULy4j5OGPWQe4LUriz+SgwT3EAz1EHET9lPBFXw9EYduQK5xZkQtsYlYWEqpmFIepSzNNvO4NgoMxmMhR7N8KZYfYtDHLIOIjdsoJSvamqaoK6sz3KZOH0neN4bRMEXjACPvUcl18lgn688psxVxHYSROSD8acjFTsAmps0rBm1ZeJ1JS2bSO0nK/0Gz/WsRZ5C1t05RJ1FSZ6pGPybjyNAcu6rcLpM+nQZwLiYNQZFZOasAlz83slYeuevaFHJpcwtx079TfEPMXHR4cSMIkZJELvpTFeVzmXonXVx7a7faqMVcfpzECpPGnh+5DhjDArtBfUMg5xCNF2OBhuJTZZO/IYpMJdu1WSJIy1e0Y3l3nNfgAxXWeMFsfoL15XrEsO4qxtzCMh4fRj6ZEZ6JQiBXGa0hGA+4aN1P1GcoBpPoheY6IkkfatDGJfyrMlXBywohYCfbTAjD2Zk9iMzGwu+zCb9U5Klc1eFc5ExGMwStdKSifLcA3Ms0yW8e4/DvbEWZe8hd16nNNlaAJ5sLs5XFsO8BirzXBGmqcATRDFNaKElnj+Spgiwe1HjZeMzUedHcMJtlfAXy8gR7iJPh/MQtRV4vRZ7pQZlhUWS29ZJ0ViQY6OG1AQ7Z2BTrkeTiosLEH9+zPAMB1EJPjkntPEgtb9t9DpdRGcCbNkOQPjCKzHbYxih9XTGzHdbsI5G7x5cFKxLc292EiWqeRIKFEHcjSjx4s6KJywEZx04Jg/cm6khVf+E0SdmHScDNjN3Dhl2D9sjIUjf4VgFjNHbP9QzRj2QUcUg9SMvEa4rZurWGyQNQoCgH50/mCVY65XmLoDBbOjIaq31GI5KR18xmaai1DNUpHr5fOCZJn0rkBmr+QA1omOWKPkDhMIoLE6tlFJiqHTJZrsBWE5T4bKbYHiDRBE6uiUKzpkrKi86kwueYbtvMcBG85rpYMFs2ufsZoixi/sGzhSR68dmqSMvFaAmzpWnObJaiBQxnt2X1LKUPicrjw2NmA8YqJM4jEoG8UORQw4VtmhTBjdrHLG+7BPU+9OjaU4h6//R6gBi7WJD2uY2G3Z/SvxVlrHOmUO59v5mt0HA22svIBaW4ZFa46P+ITT7cgP0+PsCrGmViEfJ/P2m67ZQukTXt7h+EQbXbKwWGhK8F/s0vZgPLeZ5E5mKdJOOIjdym446vYzq6gC8MfN0QdhlkYSXgc+SHRW26ZTb+G6R78f+KKN9UO89h2dL8tM8zXqyxVbOcyYfv2pOmRzp5fISy3imvTB8h0ngfrgyZr6IZPYzupGVDx8i80kG78Iyn25QBpSLRK2bLZtb/SmS07+qSCTLPzoCOze7rLIUurtnO33CxV8yWy2k4Fc2QSU541A685YOINx/m8NrQLvvMuOXYeMxF22BBmKLIH8NaCdzIbOnctBpLvIDLOizllzjzhwrlMRqvalyw9NRK0p1A9U2EZ2zqwtYWmLR+Qxo8e9PRTPIzpszbpPERAZGostdL04fKO6OA6B3920uce5Gsw79WlMdYQZchVOLGvRIGaefxWEcIrAgswO1ftJn/uCBtEJEfE/Cc2eyWGB3MIO5dlkIejhs0NNMPkOjGKlH5jD0kEeyjrcwMMCirkle7erGMsm9nFglGXDU4RdXLoPwhxZIGN1D22cdkyeyj+z33U4q8x4jYot/Ph/sFLtsyH7a/E/mhmESiH3Uy5AWzwdKZw2imRYoZS4YpzAMKeXTzDhiUfxmXXlNmA6/Xt7g0E2fLTxR5EGxxvUB28lqbBIxVMNtcBbN9QOkpYyXMxYHiLO5Qqunh5GwXJ+BNTnarMEnRZqpG3VQMP5rj4MCyCnD6Pg0PLENY1YuKsj1c3J9iF/d1AYY5hee+SlFusNsT8RZVMI1+ZsBwISwfZGYD+ijiHXRi4fL4h0aZ1j4yW1ebvhdqjC/F7Yl4CxM5uU13Si8ksfuEEK8Bq9DTx751QuMPscnPI/5+f+sEkR+gAfxcY3xVXhkQN5HGp56h8dNE7ruei/mWyn3L45sD8X4TDOiIRpsPGo7B5OG7kZnXE/lXjr3ESOLs+6WLiA8n4vO4RsHwV4y2tT0MRZAZLGNwJWke3xYFb4cfEj1/NAgh1cbQuHsfkVv5ZXa7/mjIdotLmkZhOJQ8zqDdp4joAy0uZTaWCjCakQ4qUMEo8FhmG+bh9YioMxe5ByItraA0B78xwMqkILj3Rupm8D2HqxKZaBe25WTcGcxYWEp20NYoLmoU+x2I7qq6ToMBqmwqE3G2E8XpKEMTSTGzuzCdMZEUkOYDE1tTS4b2ffCa2XhkMwZg0ZZWOmkcNbIi/uxaLv4vwADpK72JGwdfsgAAAABJRU5ErkJggg==" height="45" /><b>DeliveryGuys</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button"> <svg stroke="currentColor" fill="none"
                stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" height="1.3em"
                width="1em" xmlns="http://www.w3.org/2000/svg">
                <line x1="3" y1="12" x2="21" y2="12"></line>
                <line x1="3" y1="6" x2="21" y2="6"></line>
                <line x1="3" y1="18" x2="21" y2="18"></line>
            </svg> </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(!empty(Auth::user()->userDetail->profile_pic))
                        <img src="{{URL::to('/')}}/uploads/users/{{Auth::user()->userDetail->profile_pic}}"
                            class="user-image" alt="User Image">
                        @else
                        <img src="{{ asset('img/user2-160x160.jpg') }}" class="user-image" alt="User Image">
                        @endif
                        <span
                            class="hidden-xs">{{Auth::user()->userDetail->first_name." ".Auth::user()->userDetail->last_name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header"> @if(!empty(Auth::user()->userDetail->profile_pic))
                            <img src="{{URL::to('/')}}/uploads/users/{{Auth::user()->userDetail->profile_pic}}"
                                class="img-circle" alt="User Image">
                            @else
                            <img src="{{ asset('img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                            @endif
                            <p> Hello,
                                <small>{{Auth::user()->userDetail->first_name." ".Auth::user()->userDetail->last_name}}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left"> <a href="{{ url('myprofile') }}"
                                    class="btn btn-default btn-flat">Profile</a> </div>
                            <div class="pull-right"> <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign
                                    out</a> </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
        <!-- search form -->
    </nav>
</header>