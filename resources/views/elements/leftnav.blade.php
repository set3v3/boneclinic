<?php
$end = '';
$url = url()->current();
$url_explode = explode('/', $url);
$end = end($url_explode);
if ($end != '') {
    $activeClassMain = 'active';
}
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <div id="menu_content">
        <section class="sidebar">
            <ul class="sidebar-menu" data-widget="tree">
                <!--<li class="header">MAIN NAVIGATION</li>-->
                <li class="@if($end =='dashboard') {{$activeClassMain}} @endif"><a href="{{url('dashboard/')}}"> <i class="fa fa-tachometer-alt"></i> <span>Dashboard</span> </a></li>
                <li class="@if($end =='customers') {{$activeClassMain}} @endif"><a href="{{url('users/')}}"> <i class="fa fa-people-carry"></i> <span>Manage Customers</span> </a></li>
                <li class="@if($end =='franchisees') {{$activeClassMain}} @endif"><a href="{{url('franchisees/')}}"> <i class="fa fa-users"></i> <span>Manage Franchisees</span> </a></li>
                <li class="@if($end =='restaurants') {{$activeClassMain}} @endif"><a href="{{url('restaurants/')}}"> <i class="fa fa-hotel"></i> <span>Manage Restaurants</span> </a></li>
                <li class="@if($end =='groceries') {{$activeClassMain}} @endif"><a href="{{url('groceries/')}}"> <i class="fa fa-store"></i> <span>Manage Groceries</span> </a></li>
                <li class="@if($end =='drivers') {{$activeClassMain}} @endif"><a href="{{url('drivers/')}}"> <i class="fa fa-motorcycle"></i> <span>Manage Drivers</span> </a></li>
                <li class="@if($end =='dispatchers') {{$activeClassMain}} @endif"><a href="{{url('dispatchers/')}}"> <i class="fa fa-network-wired"></i> <span>Manage Dispatchers</span> </a></li>
                <li class="@if($end =='adminusers') {{$activeClassMain}} @endif"><a href="{{url('adminusers/')}}"> <i class="fa fa-user-cog"></i> <span>Manage Administrators</span> </a></li>
                <li class="treeview {{ ($end == 'current' || $end == 'past') ? 'active' : null }}">
                    <a href="#">
                        <i class="fa fa-truck"></i> <span>Manage Orders</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('orders/current')}}"><i class="fa fa-circle-notch"></i> Current Orders</a></li>
                        <li><a href="{{url('orders/past')}}"><i class="fa fa-circle-notch"></i> Past Orders</a></li>
                    </ul>
                </li>
                <li class="@if($end =='coupons') {{$activeClassMain}} @endif"><a href="{{url('coupons/')}}"> <i class="fa fa-grin-stars"></i> <span>Coupons</span> </a></li>
                <li class="treeview {{ ($end == 'contents' || $end == 'notificationtemplates' || $end == 'emailtemplates') ? 'active' : null }}">
                    <a href="#">
                        <i class="fa fa-sticky-note"></i> <span>Manage Contents</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('contents')}}"><i class="fa fa-circle-notch"></i> Pages </a></li>
                        <li><a href="{{url('emailtemplates')}}"><i class="fa fa-circle-notch"></i> Email Templates</a></li>
                        <li><a href="{{url('notificationtemplates')}}"><i class="fa fa-circle-notch"></i> Notification
                                Templates</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview {{ ($end == 'commissionsettings' || $end == 'tax-categories' || $end == 'roles'  || $end == 'zones'  || $end == 'tax-rates') ? 'active' : null }}">
                    <a href="#">
                        <i class="fa fa-cog"></i> <span>Settings</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <!--<li><a href="{{url('roles')}}"><i class="fa fa-circle-notch"></i> Roles </a></li>-->
                        <!--<li><a href="{{url('commissionsettings')}}"><i class="fa fa-circle-notch"></i> Commission Settings </a></li>-->
                        <li><a href="{{url('zones')}}"><i class="fa fa-circle-notch"></i> Zones </a></li>
                        <li><a href="{{url('categories')}}"><i class="fa fa-circle-notch"></i>Categories </a></li>
                        <li><a href="{{url('tax-categories')}}"><i class="fa fa-circle-notch"></i> Tax Categories </a></li>
                        <li><a href="{{url('tax-rates')}}"><i class="fa fa-circle-notch"></i> Tax Rates </a></li>
                    </ul>
                </li>
                <li class="treeview {{ ($end == 'driverearning') ? 'active' : null }}">
                    <a href="#">
                        <i class="fa fa-file"></i> <span>Reports</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{url('reports/sales')}}"><i class="fa fa-circle-notch"></i>Sales Reports </a></li>
                        <li><a href="{{url('reports/franchiseeearning')}}"><i class="fa fa-circle-notch"></i>Franchisee Earnings </a></li>
                        <li><a href="{{url('reports/driverearning')}}"><i class="fa fa-circle-notch"></i>Driver Order Earnings </a></li>
                        <li><a href="{{url('reports/driverhourlyearning')}}"><i class="fa fa-circle-notch"></i>Driver Hourly Earnings </a></li>
                    </ul>
                </li>
            </ul>
        </section>
    </div>
    <!-- /.sidebar -->
</aside>