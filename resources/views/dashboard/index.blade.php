@extends ('layouts.master')

@section('content')

<!-- Main content -->
<section class="content-header">
    <h1>
        Dashboard
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Customers</span>
                    <span class="info-box-number">10</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-hotel"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Restaurants</span>
                    <span class="info-box-number">10</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-people-carry"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Franchisee</span>
                    <span class="info-box-number">10</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        
        <!-- /.col -->
    </div>
    
</section>
<!-- /.content -->

<!-- Sparkline -->
<script src="{{ asset('global/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>

<!-- jvectormap -->
<link rel="stylesheet" href="{{ asset('global/bower_components/jvectormap/jquery-jvectormap.css') }}">
<!-- jvectormap -->
<script src="{{ asset('global/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('global/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('global/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- chart js -->
<script src="{{ asset('administrator/js/chartjs/Chart.bundle.js') }}"></script>
<script src="{{ asset('administrator/js/chartjs/utils.js') }}"></script>
<style type="text/css">
.popover {
    width: 5%;
}
</style>
<script>
$(function() {
    $('[data-toggle="tooltip"]').tooltip({
        'placement': 'top',
        'trigger': 'hover'
    })
});
</script>

@endsection