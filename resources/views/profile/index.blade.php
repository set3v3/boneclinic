@extends('layouts.master')
@section('title', 'My Profile')
@section('content')

<section class="content">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#profile" data-toggle="tab" aria-expanded="true">Profile</a></li>
            <li class=""><a href="#changepassword" data-toggle="tab" aria-expanded="false">Change Password</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="profile">
                <div style="margin-bottom:30px;" class="row">
                    <a id="enableEdit" onclick="enableEdit()" class="btn btn-info m-l-15 pull-right btn-sm"><span
                            class="Cicon"><i class="fa fa-pencil"></i></span> Edit</a>
                    <a id="disbaleEdit" onclick="disbaleEdit()" style="display:none;"
                        class="btn btn-info m-l-15 pull-right btn-sm"><span class="Cicon"><i
                                class="fa fa-eye"></i></span> View</a>
                </div>
                <form id="editProfile" action="{{ route('myprofile.update') }}" method="POST"
                    enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                    <div class="row">
                        <div class="picture-container">
                            <div class="picture">
                                @if(!empty($user->userDetail->profile_pic))
                                <img src="{{URL::to('/')}}/uploads/users/{{$user->userDetail->profile_pic}}"
                                    class="picture-src" id="wizardPicturePreview" title="">
                                @else
                                <img src="https://lh3.googleusercontent.com/LfmMVU71g-HKXTCP_QWlDOemmWg4Dn1rJjxeEsZKMNaQprgunDTtEuzmcwUBgupKQVTuP0vczT9bH32ywaF7h68mF-osUSBAeM6MxyhvJhG6HKZMTYjgEv3WkWCfLB7czfODidNQPdja99HMb4qhCY1uFS8X0OQOVGeuhdHy8ln7eyr-6MnkCcy64wl6S_S6ep9j7aJIIopZ9wxk7Iqm-gFjmBtg6KJVkBD0IA6BnS-XlIVpbqL5LYi62elCrbDgiaD6Oe8uluucbYeL1i9kgr4c1b_NBSNe6zFwj7vrju4Zdbax-GPHmiuirf2h86eKdRl7A5h8PXGrCDNIYMID-J7_KuHKqaM-I7W5yI00QDpG9x5q5xOQMgCy1bbu3St1paqt9KHrvNS_SCx-QJgBTOIWW6T0DHVlvV_9YF5UZpN7aV5a79xvN1Gdrc7spvSs82v6gta8AJHCgzNSWQw5QUR8EN_-cTPF6S-vifLa2KtRdRAV7q-CQvhMrbBCaEYY73bQcPZFd9XE7HIbHXwXYA=s200-no"
                                    class="picture-src" id="wizardPicturePreview" title="">
                                @endif
                                <input type="file" name="profile_pic" id="wizard-picture" class="">
                            </div>
                            <h6 class="">Choose Picture</h6>

                        </div>
                        <div class="form-group">
                            <label for="first_name" class="col-sm-2 control-label">First Name</label>
                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" id="first_name" name="first_name"
                                    value="{{ old('first_name', isset($user->userDetail) ? $user->userDetail->first_name : '') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="last_name" class="col-sm-2 control-label">Last Name</label>

                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" id="last_name" name="last_name"
                                    value="{{ old('last_name', isset($user->userDetail) ? $user->userDetail->last_name : '') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-sm-2 control-label">Email</label>

                            <div class="col-sm-10">
                                <input type="email" disabled class="form-control" id="email" name="email"
                                    value="{{ old('email', isset($user) ? $user->email : '') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">Phone</label>

                            <div class="col-sm-10">
                                <input type="text" disabled class="form-control" id="phone" name="phone"
                                    value="{{ old('phone', isset($user) ? $user->phone : '') }}" }">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" disabled class="btn btn-danger">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tab-pane" id="changepassword">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                <form method="POST" action="{{ route('change.password') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">Current
                                            Password</label>
                                        <div class="col-md-6">
                                            <input id="password" type="password" class="form-control"
                                                name="current_password" autocomplete="current-password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">New
                                            Password</label>
                                        <div class="col-md-6">
                                            <input id="new_password" type="password" class="form-control"
                                                name="new_password" autocomplete="current-password">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm
                                            Password</label>
                                        <div class="col-md-6">
                                            <input id="new_confirm_password" type="password" class="form-control"
                                                name="new_confirm_password" autocomplete="current-password">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                Update Password
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</section>


<script>
function enableEdit() {
    $('#disbaleEdit').show();
    $('#enableEdit').hide();
    $("#editProfile :input").prop("disabled", false);
}

function disbaleEdit() {
    $('#enableEdit').show();
    $('#disbaleEdit').hide();
    $("#editProfile :input").prop("disabled", true);
}

$(document).ready(function() {
    // Prepare the preview for profile picture
    $("#wizard-picture").change(function() {
        readURL(this);
        enableEdit();
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection